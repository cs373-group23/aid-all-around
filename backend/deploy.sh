echo "Deploying Backend ..."

aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 877290019289.dkr.ecr.us-east-2.amazonaws.com
docker build -t aaabackend .
docker tag aaabackend:latest 877290019289.dkr.ecr.us-east-2.amazonaws.com/aws-aaabackend:latest
docker push 877290019289.dkr.ecr.us-east-2.amazonaws.com/aws-aaabackend:latest
cd aws_deploy
eb deploy