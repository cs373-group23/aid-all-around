import flask
import flask_sqlalchemy
import flask_restless
import json
import requests
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.orm import relationship
from flask_cors import CORS

# Create the Flask application and the Flask-SQLAlchemy object.
app = flask.Flask(__name__)
CORS(app)

app.config["DEBUG"] = True
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql+psycopg2://postgres:aidallaround@aidallaround.c1zsy0wkakju.us-east-2.rds.amazonaws.com:5432/aidallarounddb"
db = flask_sqlalchemy.SQLAlchemy(app)


@app.route("/")
def home():
    return "ok"


class Country(db.Model):
    country_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode, default="")
    # sorting & filtering attributes
    region = db.Column(db.Unicode, default="")
    population = db.Column(db.Integer, default=0)
    latlng = db.Column(ARRAY(db.Float))
    latitude = db.Column(db.Float, default=0.0)
    longitude = db.Column(db.Float, default=0.0)
    gini = db.Column(db.Float, default=0.0)
    incomeLevel = db.Column(db.Unicode, default="")
    # other attributes
    currencies = db.Column(db.Unicode, default="")
    languages = db.Column(db.Unicode, default="")
    area = db.Column(db.Float, default=0.0)
    timezones = db.Column(ARRAY(db.Unicode))
    alpha2Code = db.Column(db.Unicode, default="")
    flag = db.Column(db.Unicode, default="")
    capital = db.Column(db.Unicode, default="")
    # links
    country_charity_id = db.Column(db.Integer)
    country_article_id = db.Column(db.Integer)


class Charity(db.Model):
    charity_id = db.Column(db.Integer, primary_key=True)
    charityName = db.Column(db.Unicode, default="")
    # sorting & filtering attributes
    nteeClassification = db.Column(db.Unicode, default="")
    incomeAmount = db.Column(db.Float, default=0.0)
    assetAmount = db.Column(db.Float, default=0.0)
    causeName = db.Column(db.Unicode, default="")
    rating = db.Column(db.Integer, default=0)
    # other attributes
    websiteURL = db.Column(db.Unicode, default="")
    ratingImage = db.Column(db.Unicode, default="")
    mailingAddress = db.Column(db.JSON)
    nteeType = db.Column(db.Unicode, default="")
    causeImage = db.Column(db.Unicode, default="")
    mission = db.Column(db.Unicode, default="")
    tagLine = db.Column(db.Unicode, default="")
    ein = db.Column(db.Unicode, unique=True, default="")
    # links
    charity_country_id = db.Column(db.Integer)
    charity_article_id = db.Column(db.Integer)


class Article(db.Model):
    article_id = db.Column(db.Integer, primary_key=True)
    # sorting & filtering attributes
    articleName = db.Column(db.Unicode, default="")
    sourceName = db.Column(db.Unicode, default="")
    reportedCountry = db.Column(db.Unicode, default="")
    publishedAt = db.Column(db.Unicode, default="")
    sourceCountry = db.Column(db.Unicode, default="")
    category = db.Column(db.Unicode, default="")
    # other attributes
    language = db.Column(db.Unicode, default="")
    websiteURL = db.Column(db.Unicode, default="")
    description = db.Column(db.Unicode, default="")
    articleImage = db.Column(db.Unicode, default="")
    articleAuthor = db.Column(db.Unicode, default="")
    articleSnippet = db.Column(db.Unicode, default="")
    # links
    article_country_id = db.Column(db.Integer)
    article_charity_id = db.Column(db.Integer)


db.create_all()
manager = flask_restless.APIManager(app, flask_sqlalchemy_db=db)
manager.create_api(Country, methods=["GET", "POST"])
manager.create_api(Charity, methods=["GET", "POST"], results_per_page=0)
manager.create_api(Article, methods=["GET", "POST"])


if __name__ == "__main__":
    app.run(port=8080)
