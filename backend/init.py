import flask
import flask_sqlalchemy
import flask_restless

# Create the Flask application and the Flask-SQLAlchemy object.
app = flask.Flask(__name__)
app.config["DEBUG"] = True
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql+psycopg2://postgres:aidallaround@aidallaround.c1zsy0wkakju.us-east-2.rds.amazonaws.com:5432/aidallarounddb"
db = flask_sqlalchemy.SQLAlchemy(app)
