from main import app
import unittest


class TestBackend(unittest.TestCase):
    def test_country_response(self):
        client = app.test_client(self)
        response = client.get("/api/country")
        self.assertEqual(response.status_code, 200)

    def test_country_content(self):
        client = app.test_client(self)
        response = client.get("/api/country")
        self.assertEqual(response.content_type, "application/json")

    def test_country_id_response(self):
        client = app.test_client(self)
        response = client.get("/api/country/1")
        self.assertEqual(response.status_code, 200)

    def test_country_id_content(self):
        client = app.test_client(self)
        response = client.get("/api/country/1")
        self.assertEqual(response.content_type, "application/json")

    def test_charity_response(self):
        client = app.test_client(self)
        response = client.get("/api/charity")
        self.assertEqual(response.status_code, 200)

    def test_charity_content(self):
        client = app.test_client(self)
        response = client.get("/api/charity")
        self.assertEqual(response.content_type, "application/json")

    def test_charity_id_response(self):
        client = app.test_client(self)
        response = client.get("/api/charity/1")
        self.assertEqual(response.status_code, 200)

    def test_charity_id_content(self):
        client = app.test_client(self)
        response = client.get("/api/charity/1")
        self.assertEqual(response.content_type, "application/json")

    def test_article_response(self):
        client = app.test_client(self)
        response = client.get("/api/article")
        self.assertEqual(response.status_code, 200)

    def test_article_content(self):
        client = app.test_client(self)
        response = client.get("/api/article")
        self.assertEqual(response.content_type, "application/json")

    def test_article_id_response(self):
        client = app.test_client(self)
        response = client.get("/api/article/1")
        self.assertEqual(response.status_code, 200)

    def test_article_id_content(self):
        client = app.test_client(self)
        response = client.get("/api/article/1")
        self.assertEqual(response.content_type, "application/json")

    def test_filtered_country_response(self):
        client = app.test_client(self)
        response = client.get(
            '/api/country?q={"filters":[{"name":"name","op":"ilike","val":"Afghanistan"}]}'
        )
        self.assertEqual(response.status_code, 200)

    def test_sorted_country_response(self):
        client = app.test_client(self)
        response = client.get(
            '/api/country?q={"order_by":[{"field":"area","direction":"desc"}]}'
        )
        self.assertEqual(response.status_code, 200)

    def test_filtered_charity_response(self):
        client = app.test_client(self)
        response = client.get(
            '/api/charity?q={"filters":[{"name":"charityName","op":"ilike","val":"Safe Passage"}]}'
        )
        self.assertEqual(response.status_code, 200)

    def test_sorted_charity_response(self):
        client = app.test_client(self)
        response = client.get(
            '/api/charity?q={"order_by":[{"field":"assetAmount","direction":"desc"}]}'
        )
        self.assertEqual(response.status_code, 200)

    def test_filtered_article_response(self):
        client = app.test_client(self)
        response = client.get(
            '/api/article?q={"filters":[{"name":"articleName","op":"ilike","val":"%Guatemalan%"}]}'
        )
        self.assertEqual(response.status_code, 200)

    def test_sorted_article_response(self):
        client = app.test_client(self)
        response = client.get(
            '/api/article?q={"order_by":[{"field":"publishedAt","direction":"desc"}]}'
        )
        self.assertEqual(response.status_code, 200)


if __name__ == "__main__":
    unittest.main()
