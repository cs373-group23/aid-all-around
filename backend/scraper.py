import requests
import json
from main import db, Country, Charity, Article
import pickle

country_charity_dict = {
    "Guatemala": "Safe Passage",
    "Israel": "StandWithUs",
    "India": "The Akshaya Patra Foundation USA",
    "Afghanistan": "Morning Star Development",
    "Angola": "World Bicycle Relief",
    "Armenia": "Children of Armenia Fund",
    "Australia": "Cultural Survival",
    "Bahamas": "The Lyford Cay Foundation",
    "Bangladesh": "BRAC USA",
    "Belarus": "Project Kesher",
    "Belize": "Children's Cup",
    "Benin": "Solar Electric Light Fund",
    "Bermuda": "Bermuda Institute of Ocean Sciences",
    "Bhutan": "The Tibet Fund",
    "Bolivia (Plurinational State of)": "Mano a Mano International",
    "Brazil": "BrazilFoundation",
    "Bulgaria": "All God's Children International",
    "Burkina Faso": "Kinship United",
    "Burundi": "Village Health Works",
    "Cambodia": "Caring for Cambodia",
    "Canada": "Quebec-Labrador Foundation",
    "Central African Republic": "Water for Good, Inc.",
    "Chad": "Neverthirst",
    "China": "China Institute in America",
    "Colombia": "Give to Colombia",
    "Congo": "Heal Africa USA",
    "Costa Rica": "Organization for Tropical Studies",
    "Cuba": "MEDICC",
    "Czech Republic": "CERGE-EI Foundation",
    "Denmark": "American-Scandinavian Foundation",
    "Dominican Republic": "Solid Rock International",
    "Ecuador": "Extreme Response International",
    "Egypt": "Coptic Orphans",
    "El Salvador": "Salvadoran American Humanitarian Foundation",
    "Equatorial Guinea": "World Link Ministries",
    "Ethiopia": "A Chance In Life",
    "Fiji": "Students International",
    "France": "French-American Foundation",
    "Gambia": "Operation Bootstrap Africa",
    "Germany": "American Council on Germany",
    "Ghana": "Foundation of Orthopedics and Complex Spine",
    "Guyana": "Sewa International, Inc",
    "Haiti": "Haitian Education & Leadership Program",
    "Honduras": "El Hogar Ministries",
    "Indonesia": "United States-Indonesia Society",
    "Iran (Islamic Republic of)": "National Iranian American Council",
    "Iraq": "Life for Relief and Development",
    "Ireland": "The Ireland Funds America",
    "Italy": "American Italian Cancer Foundation",
    "Jamaica": "Caribbean Christian Centre for the Deaf",
    "Japan": "Japan Society",
    "Jordan": "Anera",
    "Kenya": "KickStart International",
    "Lao People's Democratic Republic": "Thrive Networks",
    "Latvia": "Bridge Builders International",
    "Lebanon": "Heart for Lebanon Foundation",
    "Liberia": "Curamericas Global",
    "Madagascar": "Health In Harmony",
    "Malawi": "GAIA",
    "Mali": "buildOn",
    "Mexico": "Project Mexico & St. Innocent Orphanage",
    "Micronesia (Federated States of)": "Canvasback Missions, Inc.",
    "Moldova (Republic of)": "FRIENDS in Action International",
    "Mongolia": "G.O. Ministries",
    "Morocco": "Education For Employment",
    "Mozambique": "Audio Scripture Ministries",
    "Myanmar": "ASAP Ministries",
    "Namibia": "Cheetah Conservation Fund",
    "Nepal": "BlinkNow Foundation",
    "Netherlands": "Netherland-America Foundation",
    "Nicaragua": "Fabretto",
    "Nigeria": "IHCF African Christian Hospitals",
    "Korea (Democratic People's Republic of)": "Liberty in North Korea",
    "Norway": "Partners Relief and Development",
    "Pakistan": "Developments in Literacy",
    "Palestine, State of": "Palestine Children's Relief Fund",
    "Panama": "Sea Turtle Conservancy",
    "Peru": "Amazon Watch",
    "Philippines": "Children's Shelter of Cebu",
    "Portugal": "The Blue Army of Our Lady of Fatima, U.S.A.",
    "Romania": "Harvest International",
    "Russian Federation": "Russian Children's Welfare Society",
    "Rwanda": "Keep a Child Alive",
    "Senegal": "Near East Foundation",
    "Sierra Leone": "Children of the Nations",
    "South Africa": "Shared Interest",
    "Korea (Republic of)": "The Korea Society",
    "South Sudan": "Water for South Sudan",
    "Sri Lanka": "Seacology",
    "Sudan": "Sudan Relief Fund",
    "Swaziland": "The Luke Commission Inc",
    "Syrian Arab Republic": "Mercy Without Limits",
    "Taiwan": "Christian Salvation Service",
    "Tajikistan": "Central Asia Institute",
    "Tanzania, United Republic of": "The Outreach Program",
    "Thailand": "Soi Dog USA",
    "Turkey": "Strategic Resource Group",
    "Uganda": "Village Enterprise",
    "Ukraine": "Ukrainian Catholic Education Foundation",
    "United Kingdom of Great Britain and Northern Ireland": "Virgin Unite USA",
    "United States of America": "The National Security Archive",
    "Uzbekistan": "International Labor Rights Forum",
    "Viet Nam": "Worldwide Orphans Foundation",
    "Zambia": "The Nature Conservancy",
    "Zimbabwe": "American Foundation For Children With AIDS",
}


##########################Scraper for charity############################
# make request to api and set the json equal to data
def store_charities():
    api_url = "https://api.data.charitynavigator.org/v2/Organizations?app_id=1f266e9a&app_key=cda2f91d4ce66c03cca86ff9f61fbb85&pageSize=1000&pageNum=1&rated=true&scopeOfWork=INTERNATIONAL"
    api_url2 = "https://api.data.charitynavigator.org/v2/Organizations?app_id=1f266e9a&app_key=cda2f91d4ce66c03cca86ff9f61fbb85&pageSize=1000&pageNum=2&rated=true&scopeOfWork=INTERNATIONAL"
    r = requests.get(api_url)
    r2 = requests.get(api_url2)

    return r.json(), r2.json()


# push to db
def push_charities(charitiesData):
    for data in charitiesData:
        myDict = dict()

        myDict["charityName"] = data["charityName"]
        myDict["nteeClassification"] = data["irsClassification"]["nteeClassification"]
        myDict["incomeAmount"] = data["irsClassification"]["incomeAmount"]
        myDict["assetAmount"] = data["irsClassification"]["assetAmount"]
        myDict["causeName"] = data["cause"]["causeName"]

        myDict["websiteURL"] = data["websiteURL"]

        if "currentRating" not in json.dumps(data):
            myDict["rating"] = 0
            myDict["ratingImage"] = "unknown"
        else:
            myDict["rating"] = data["currentRating"]["rating"]
            myDict["ratingImage"] = data["currentRating"]["ratingImage"]["large"]

        myDict["mailingAddress"] = data["mailingAddress"]
        myDict["nteeType"] = data["irsClassification"]["nteeType"]
        myDict["causeImage"] = data["cause"]["image"]
        myDict["mission"] = data["mission"]
        myDict["tagLine"] = data["tagLine"]
        myDict["ein"] = data["ein"]

        country = ""
        target_country = ""
        for c in country_charity_dict.keys():
            if country_charity_dict[c] == myDict["charityName"]:
                target_country = c
                country = (
                    db.session.query(Country)
                    .filter(Country.name == target_country)
                    .first()
                )
                if country != None:
                    myDict["charity_country_id"] = country.country_id
                    myDict["charity_article_id"] = country.country_article_id
                else:
                    print(c)

        if target_country == "":
            continue

        # check and see if charity already exists in db using ein
        charity_db_instance = Charity(**myDict)
        db.session.add(charity_db_instance)
        db.session.commit()

        charity = (
            db.session.query(Charity)
            .filter(Charity.charity_country_id == myDict["charity_country_id"])
            .first()
        )
        country.country_charity_id = charity.charity_id

        article = (
            db.session.query(Article)
            .filter(Article.article_country_id == myDict["charity_country_id"])
            .first()
        )
        article.article_charity_id = charity.charity_id

        db.session.commit()


def store_and_push_charities():
    charitiesData1, charitiesData2 = store_charities()
    push_charities(charitiesData1)
    push_charities(charitiesData2)


##########################Scraper for country############################
# make request to api and set the json equal to data
def store_countries():
    api_url = "https://restcountries.eu/rest/v2/all"
    r = requests.get(api_url)

    return r.json()


# push to db
def push_countries(countriesData, income_level_dict):
    for data in countriesData:
        myDict = dict()

        myDict["name"] = data["name"]
        myDict["region"] = data["region"]
        myDict["population"] = data["population"]
        myDict["latlng"] = data["latlng"]
        if len(data["latlng"]) != 0:
            myDict["latitude"] = data["latlng"][0]
            myDict["longitude"] = data["latlng"][1]
        else:
            myDict["latitude"] = None
            myDict["longitude"] = None

        myDict["gini"] = data["gini"]
        if data["alpha2Code"] in income_level_dict.keys():
            myDict["incomeLevel"] = income_level_dict[data["alpha2Code"]]
        else:
            print(data["name"] + ": no income level")

        myDict["currencies"] = data["currencies"][0]["code"]
        myDict["languages"] = data["languages"][0]["name"]
        myDict["area"] = data["area"]
        myDict["timezones"] = data["timezones"]
        myDict["alpha2Code"] = data["alpha2Code"]
        myDict["flag"] = data["flag"]
        myDict["capital"] = data["capital"]

        country_db_instance = Country(**myDict)
        db.session.add(country_db_instance)
        db.session.commit()


def get_country_income_levels():
    income_level_dict = dict()

    # for all World Bank countries: page 1-5: 50 countries, page 6: 47 countries
    for page_num in range(1, 7):
        api_url = "http://api.worldbank.org/v2/country/?format=json&page=" + str(
            page_num
        )
        r = requests.get(api_url)

        for data in r.json()[1]:
            income_level_dict[data["iso2Code"]] = data["incomeLevel"]["value"]

    return income_level_dict


def store_and_push_countries():
    countriesData = store_countries()
    income_level_dict = get_country_income_levels()
    push_countries(countriesData, income_level_dict)


def gatherNewsForCountries(sources):
    with open("news.txt", "rb") as fp:  # Unpickling
        sources = pickle.load(fp)
    l = ["Bolivia"]
    for country in l:
        list_of_possibilities = "&sources=abc-news,associated-press,bbc-news,cbs-news,cnn,fox-news,google-news,msnbc,nbc-news,newsweek,reuters,the-wall-street-journal,the-washington-post,time,usa-today,vice-news,the-huffington-post,reddit-r-all,national-geographic,medical-news-today"
        api_url = (
            'https://newsapi.org/v2/everything?q="'
            + country
            + '"'
            + list_of_possibilities
            + "&sortBy=relevancy&apiKey=c087c99b9d214617b08ec973d45a2875&page=1"
        )
        r = requests.get(api_url)
        source = r.json()["articles"]
        sources[country] = source
        print(country)
        with open("news.txt", "wb") as fp:
            pickle.dump(sources, fp)


def gatherAndStoreOrganizations(organizations):
    api_url = "https://newsapi.org/v2/sources?apiKey=c087c99b9d214617b08ec973d45a2875"
    r = requests.get(api_url)
    all_organizations = r.json()

    sources_dict = {}

    for o in all_organizations["sources"]:
        sources_dict[o["name"]] = o

    with open("organizations.txt", "wb") as fp:
        pickle.dump(sources_dict, fp)


# ##########################Scraper for news source ############################
# make request to api and set the json equal to data


def store_sources():
    sources = {}

    with open("news.txt", "rb") as fp:  # Unpickling
        sources = pickle.load(fp)

    # fix bad country names
    sources["Iran (Islamic Republic of)"] = sources["Iran"]
    del sources["Iran"]
    sources["Lao People's Democratic Republic"] = sources["Laos"]
    del sources["Laos"]
    sources["Micronesia (Federated States of)"] = sources["Micronesia"]
    del sources["Micronesia"]
    sources["Moldova (Republic of)"] = sources["Moldova"]
    del sources["Moldova"]
    sources["Korea (Democratic People's Republic of)"] = sources["North Korea"]
    del sources["North Korea"]
    sources["Palestine, State of"] = sources["Palestine"]
    del sources["Palestine"]
    sources["Russian Federation"] = sources["Russia"]
    del sources["Russia"]
    sources["Korea (Republic of)"] = sources["South Korea"]
    del sources["South Korea"]
    sources["Syrian Arab Republic"] = sources["Syria"]
    del sources["Syria"]
    sources["Tanzania, United Republic of"] = sources["Tanzania"]
    del sources["Tanzania"]
    sources["United Kingdom of Great Britain and Northern Ireland"] = sources[
        "United Kingdom"
    ]
    del sources["United Kingdom"]
    sources["Viet Nam"] = sources["Vietnam"]
    del sources["Vietnam"]
    sources["Bolivia (Plurinational State of)"] = sources["Bolivia"]
    del sources["Bolivia"]

    orgnaizations = {}

    with open("organizations.txt", "rb") as fp:  # Unpickling
        orgnaizations = pickle.load(fp)

    return sources, orgnaizations


def push_sources(articles, organizations):
    for c in articles.keys():
        myDict = dict()

        if len(articles[c]) == 0:
            continue

        my_article = articles[c][0]

        myDict["articleName"] = my_article["title"]
        myDict["sourceName"] = my_article["source"]["name"]
        myDict["reportedCountry"] = c
        myDict["publishedAt"] = my_article["publishedAt"]

        if organizations[my_article["source"]["name"]]["country"] == "gb":
            myDict["sourceCountry"] = "Great Britain"
        else:
            myDict["sourceCountry"] = "United States"

        myDict["category"] = organizations[my_article["source"]["name"]]["category"]
        myDict["language"] = organizations[my_article["source"]["name"]]["language"]
        myDict["websiteURL"] = my_article["url"]
        myDict["description"] = my_article["description"]
        myDict["articleImage"] = my_article["urlToImage"]
        myDict["articleAuthor"] = my_article["author"]
        myDict["articleSnippet"] = my_article["content"]

        country = db.session.query(Country).filter(Country.name == c).first()

        if country != None:
            myDict["article_country_id"] = country.country_id
        else:
            print(c)

        source_db_instance = Article(**myDict)
        db.session.add(source_db_instance)
        db.session.commit()

        source = (
            db.session.query(Article)
            .filter(Article.article_country_id == country.country_id)
            .first()
        )
        country.country_article_id = source.article_id
        db.session.commit()


def store_and_push_sources():
    articles, organizations = store_sources()
    push_sources(articles, organizations)


store_and_push_countries()
store_and_push_sources()
store_and_push_charities()
