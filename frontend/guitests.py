import unittest
import time
from selenium import webdriver

class GUITests(unittest.TestCase):
    def setUp(self):
        # create a new remote Chrome session
        self.driver = webdriver.Remote(command_executor="http://selenium__standalone-chrome:4444/wd/hub",  options=webdriver.ChromeOptions())
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()

        # navigate to the application home page
        self.driver.get("http://www.aidallaround.me/")

    def tearDown(self):
        self.driver.quit()
    
    def test_splash(self):
        assert "Welcome to Aid All Around!" in self.driver.page_source
    
    def test_countries(self):
        self.driver.find_element_by_link_text("Countries").click()
        time.sleep(1)
        assert "Afghanistan" in self.driver.page_source
    
    def test_country(self):
        self.driver.find_element_by_link_text("Countries").click()
        time.sleep(1)
        self.driver.find_element_by_link_text("See Details").click()
        time.sleep(1)
        assert "Capital" in self.driver.page_source

    def test_charities(self):
        self.driver.find_element_by_link_text("Charities").click()
        time.sleep(5)
        assert "Safe Passage" in self.driver.page_source

    def test_charity(self):
        # can't do it directly from charities model page as MaterialTable messes with finding the link
        self.driver.get("http://www.aidallaround.me/charities/1")
        time.sleep(2)
        assert "City" in self.driver.page_source
    
    def test_articles(self):
        self.driver.find_element_by_link_text("Articles").click()
        time.sleep(1)
        assert "Grief as Guatemalan migrants killed in Mexico laid to rest" in self.driver.page_source

    def test_article(self):
        self.driver.find_element_by_link_text("Articles").click()
        time.sleep(1)
        self.driver.find_element_by_link_text("See Details").click()
        time.sleep(1)
        assert "Reported Country" in self.driver.page_source
    
    def test_about(self):
        self.driver.find_element_by_link_text("About").click()
        time.sleep(1)
        assert "Purpose" in self.driver.page_source
    
    def test_search(self):
        self.driver.find_element_by_css_selector('#inlineFormInput').send_keys("United States")
        time.sleep(2)
        self.driver.find_element_by_xpath('//*[@id="button"]').click()
        time.sleep(5)
        assert "United" in self.driver.page_source
        assert "States" in self.driver.page_source

    def test_sort(self):
        self.driver.find_element_by_link_text("Countries").click()
        time.sleep(5)
        self.driver.find_element_by_xpath('/html/body/div/div/div[3]/div[1]/div/div/div[1]/input[1]').click()
        time.sleep(2)
        self.driver.find_element_by_css_selector('#basic-typeahead-single-item-0').click()
        time.sleep(5)
        assert "China" in self.driver.page_source

    def test_filter(self):
        self.driver.find_element_by_link_text("Countries").click()
        time.sleep(5)
        self.driver.find_element_by_css_selector('#root > div > div.row.row-cols-sm-3 > div:nth-child(2) > div > div > div.rbt-input-multi.form-control.rbt-input > div > div > input.rbt-input-main').click()
        time.sleep(2)
        self.driver.find_element_by_css_selector('#token-example-item-0').click()
        time.sleep(5)
        assert "Burkina Faso" in self.driver.page_source


if __name__ == "__main__":
    unittest.main()