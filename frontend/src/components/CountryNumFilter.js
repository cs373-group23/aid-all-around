import { Dropdown } from 'react-bootstrap'
import { Form } from 'react-bootstrap'
import { Typeahead } from 'react-bootstrap-typeahead'
import { useEffect, useState } from 'react';
import 'react-bootstrap-typeahead/css/Typeahead.css';

const CountryNumFilter = (props) => {
    const [singleSelections, setSingleSelections] = useState("");
    const [multiSelections, setMultiSelections] = useState([]);

    let filters = [...props.filters]

    return (
        <Form.Group >
            <Form.Label>Filter By {props.name}</Form.Label>
            <Typeahead
                id="token-example"
                labelKey="name"
                multiple
                onChange={(e) => {
                    setMultiSelections(e)
                    props.setFilters([])
                    filters = []
                    var arrayLength = e.length;
                    for (var i = 0; i < arrayLength; i++) {
                        var vals = e[i].split(" ")
                        filters.push({"and": [{ "name": props.name, "op": "gte", "val": parseFloat(vals[0])}, { "name": props.name, "op": "lte", "val": parseFloat(vals[1])}]})
                    }
                    props.setFilters(filters)
                }
                }
                options={props.options}
                placeholder={props.name}
                selected={multiSelections}
            />
        </Form.Group>
    )
}

export default CountryNumFilter;