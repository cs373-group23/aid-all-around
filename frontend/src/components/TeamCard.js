import { Card } from 'react-bootstrap'
import styles from '../views/about/About.module.css'

const TeamCard = (props) => {
    return (
    <Card className = {styles.card_class} style={{width: '100%'}}>
        <Card.Img className = {styles.circle_headshot} src={props.src} />
        <Card.Body>
            <Card.Title>{props.name}</Card.Title>
            <Card.Text>{props.bio}</Card.Text>
            <Card.Text>Major Responsibilities: {props.responsibilities}</Card.Text>
            <Card.Text>Commits: {props.commits}</Card.Text>
            <Card.Text>Issues: {props.issues}</Card.Text>
            <Card.Text>Unit Tests: {props.tests}</Card.Text>
        </Card.Body>
    </Card>
    )
}

export default TeamCard