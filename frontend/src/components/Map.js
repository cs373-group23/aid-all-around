import React, { Component } from 'react'
import { MapContainer, TileLayer, CircleMarker, Popup } from 'react-leaflet'
import 'leaflet/dist/leaflet.css';

const Map = (props) => {

  const position = [props.latitude, props.longitude]

  return (
    <MapContainer style={{ height: "450px", width: "100%" }} center={position} zoom={props.zoom}>
      <TileLayer
        attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <CircleMarker center={[props.latitude, props.longitude]} pathOptions={{ color: 'red', fillColor: 'red' }} radius={15}></CircleMarker>
    </MapContainer>
  )
}

export default Map

