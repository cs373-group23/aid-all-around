import { Dropdown } from 'react-bootstrap'
import { Form } from 'react-bootstrap'
import { Typeahead } from 'react-bootstrap-typeahead'
import { useEffect, useState } from 'react';

const CountrySort = (props) => {
    const [singleSelections, setSingleSelections] = useState("");
    const [multiSelections, setMultiSelections] = useState([]);
    const [options, setOptions] = useState([]);
    let queries = [...props.queries]

    const opts = []
    props.options.map(element => 
        {
            opts.push(element + "," + "desc")
            opts.push(element + "," + "asc")
        }
    )

    return (
        <Form.Group>
            <Form.Label>Sort By</Form.Label>
            <Typeahead
                id="basic-typeahead-single"
                labelKey="name"
                onChange={(e) => {
                    var q = props.options

                    if (opts.includes(e[0])){
                        var field = e[0].split(",")[0]
                        var direction = e[0].split(",")[1]
                        props.setQueries([])
                        queries = []
                        queries.push({ "field": field, "direction": direction })
                        props.setQueries(queries)
                    }
                }}
                options={opts}
                placeholder="Sort by..."
                selected={singleSelections}
            />
        </Form.Group>
    )
}

export default CountrySort;