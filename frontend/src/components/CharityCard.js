import { Card, Button } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';

const CharityCard = (props) => {
    const data = props.data

    return (
        <Card style={{width: '90%', lineHeight: '95%'}}>
            <Card.Img variant="top" src={data.causeImage} style={{
                objectFit: 'cover',
                width: '100%',
                height: '25vh'
            }} />

            <Card.Body>
                <Card.Title>{data.charityName}</Card.Title>
                <Card.Text>Classification: {data.nteeClassification}</Card.Text>
                <Card.Text>Income Amount: {data.incomeAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Card.Text>
                <Card.Text>Asset Amount: {data.assetAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Card.Text>
                <Card.Text>Cause Name: {data.causeName.charAt(0).toUpperCase() + data.causeName.slice(1)}</Card.Text>
                <Card.Text>Rating: {data.rating} (out of 4) </Card.Text>

            </Card.Body>
            <Button href={`/charities/${data.charity_id}`} variant="primary">See Details</Button>
        </Card>
    )
}

export default CharityCard