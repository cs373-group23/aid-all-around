import 'bootstrap/dist/css/bootstrap.min.css';

// a row of the table for each charity
const CharityComponent = (props) => {
    const data = props.data

    return (
        <tr>
            <td>{data.charity_id}</td>
            <td>{data.charityName}</td>
            <td><a href="/1">{data.charityName}</a></td>
            <td>{data.nteeClassification}</td>
            <td>{data.incomeAmount}</td>
            <td>{data.assetAmount}</td>
            <td>{data.causeName}</td>
            <td>{data.rating}</td>
        </tr>
    )
}

export default CharityComponent