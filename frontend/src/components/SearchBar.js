import { Form, FormControl, Button, Row, Col, Container } from 'react-bootstrap'
import React, { useState } from 'react'
import {Link} from 'react-router-dom'

// Component for the search bar of the website
const SearchBar = (props) => {
  const [queryString, setQueryString] = useState([])
  
  return (
    <Form>
      <Form.Row className="align-items-center">
        <Col xs="7">
          <Form.Label htmlFor="inlineFormInput" srOnly>
            Search
          </Form.Label>
          <Form.Control
            className="mb-2"
            id="inlineFormInput"
            placeholder="Search"
            onChange={(e) => {
              setQueryString(e.target.value)
            }}
          />
        </Col>
        <Col xs="auto">
          <Link to={`/search?q=${queryString}&b=${props.modelString}`} className="btn btn-primary">Search</Link>
        </Col>
      </Form.Row>
    </Form>
  )
}

export default SearchBar;