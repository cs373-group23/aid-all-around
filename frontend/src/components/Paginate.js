import { Pagination , OverlayTrigger, Tooltip} from 'react-bootstrap'

const renderFirst = (props) => (
    <Tooltip id="button-tooltip" {...props}>
        First
    </Tooltip>
);

const Paginate = (props) => {
    return (
		<Pagination>

            <OverlayTrigger
                placement="top"
                delay={{ show: 250, hide: 400 }}
                overlay={
                    <Tooltip id={`button-tooltip`}>
                        First
                    </Tooltip>
                }>
            <Pagination.First onClick={() => {
                props.setPage(1)
            }} />
            </OverlayTrigger> 

            <OverlayTrigger
                placement="top"
                delay={{ show: 250, hide: 400 }}
                overlay={
                    <Tooltip id={`button-tooltip`}>
                        Prev
                    </Tooltip>
                }>
            <Pagination.Prev disabled={props.page - 1 <= 0} onClick={() => {
                props.setPage(props.page - 1)
            }} />
            </OverlayTrigger> 

            <Pagination.Item active onClick={() => {
                if (props.page > 0) {
                    props.setPage(props.page)
                }
            }}>{props.page}</Pagination.Item>

            {props.page + 1 <= props.limit ?
                <Pagination.Item onClick={() => {
                    props.setPage(props.page + 1)
                }}>{props.page + 1}</Pagination.Item>
                : undefined
            }

            {props.page + 2 <= props.limit ?
                <Pagination.Item onClick={() => {
                    props.setPage(props.page + 2)
                }}>{props.page + 2}</Pagination.Item>
                : undefined
            }

            {props.page + 3 <= props.limit ?
                <Pagination.Item onClick={() => {
                    props.setPage(props.page + 3)

                }}>{props.page + 3}</Pagination.Item>
                : undefined
            }

            {props.page + 4 <= props.limit ?
                <Pagination.Item onClick={() => {
                    props.setPage(props.page + 4)
                }}>{props.page + 4}</Pagination.Item>
                : undefined
            }

            <OverlayTrigger
                placement="top"
                delay={{ show: 250, hide: 400 }}
                overlay={
                    <Tooltip id={`button-tooltip`}>
                        Next
                    </Tooltip>
                }>
            <Pagination.Next disabled={props.page + 1 > props.limit} onClick={() => {
                    props.setPage(props.page + 1)
            }} />
            </OverlayTrigger>

            <OverlayTrigger
                placement="top"
                delay={{ show: 250, hide: 400 }}
                overlay={
                    <Tooltip id={`button-tooltip`}>
                        Last
                    </Tooltip>
                }>
            <Pagination.Last onClick={() => {
                props.setPage(props.limit)
            }} />
            </OverlayTrigger>       

        </Pagination>
    )
}

export default Paginate