import { Card, Button } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import Highlighter from "react-highlight-words";
import { Link } from 'react-router-dom'

const CountrySearchCard = (props) => {
    const data = props.data
    var lat = data.latitude + "°N"
    if (data.latitude < 0) {
        lat = -data.latitude + "°S"
    }
    var long = data.longitude + "°E"
    if (data.longitude < 0) {
        long = -data.longitude + "°W"
    }
    return (
        <Card style={{width: '90%', lineHeight: '95%'}} >
            
            <Card.Body>
                <Link to={`/countries/${data.country_id}`} className="link">
                <Card.Title>
                    <Highlighter
                        highlightClassName="bg-success"
                        searchWords={props.query}
                        autoEscape={true}
                        textToHighlight={data.name}
                    />
                </Card.Title>
                </Link>
                <Card.Text>Region: {" "}
                <Highlighter
                        highlightClassName="bg-success"
                        searchWords={props.query}
                        autoEscape={true}
                        textToHighlight={data.region}
                    />   
                </Card.Text>

                <Card.Text>Population: {data.population.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Card.Text>
                <Card.Text>Lat/Long: {lat + `, ` + long}</Card.Text>
                <Card.Text>Income Level: {" "}
                <Highlighter
                        highlightClassName="bg-success"
                        searchWords={props.query}
                        autoEscape={true}
                        textToHighlight={data.incomeLevel}
                    />   
                </Card.Text>
                <Card.Text><a href="https://en.wikipedia.org/wiki/Gini_coefficient">Gini: </a>{data.gini}</Card.Text>
            </Card.Body>
        </Card>
    )
}

export default CountrySearchCard


