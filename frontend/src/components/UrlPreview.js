import { Card, Container } from 'react-bootstrap'
import React, { useEffect, useState } from 'react'
import cheerio from 'cheerio'

const UrlPreview = (props) => {
    const websiteUrl = props.url
    const [img, setImg] = useState(props.img)
    const [name, setName] = useState(props.url)
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        setLoading(false)
    }, [])

    return (
        <Container style={{
            display: 'flex',
            justifyContent: 'center'
        }}>
            {!loading ? (
                <Card as="a" onClick={(e) => {
                    window.location.href = websiteUrl
                }} style={{
                    width: '50%'
                }}>
                    <Card.Img variant="top" src={img} style={{
                        objectFit: 'cover',
                        width: '100%',
                        height: '25vh'
                    }} />
                    <Card.Body>
                        <Card.Title>{name}</Card.Title>
                    </Card.Body>
                </Card>
            ) : undefined}
        </Container>
    )
}

export default UrlPreview