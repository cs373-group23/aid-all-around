import { Card, Button } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';

const CountryCard = (props) => {
    const data = props.data
    var lat = data.latitude + "°N"
    if (data.latitude < 0) {
        lat = -data.latitude + "°S"
    }
    var long = data.longitude + "°E"
    if (data.longitude < 0) {
        long = -data.longitude + "°W"
    }

    return (
        <Card style={{width: '90%', lineHeight: '95%'}}>
            <Card.Img variant="top" src={data.flag} style={{
                objectFit: 'cover',
                width: '100%',
                height: '25vh'
            }} />
            <Card.Body>
                <Card.Title>{data.name}</Card.Title>
                <Card.Text>Region: {data.region}</Card.Text>
                <Card.Text>Population: {data.population.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Card.Text>
                <Card.Text>Lat/Long: {lat + `, ` + long}</Card.Text>
                <Card.Text>Income Level: {data.incomeLevel}</Card.Text>
                <Card.Text><a href="https://en.wikipedia.org/wiki/Gini_coefficient">Gini: </a>{data.gini}</Card.Text>
            </Card.Body>
            <Button href={`/countries/${data.country_id}`} variant="primary">See Details</Button>
        </Card>
    )
}

export default CountryCard


