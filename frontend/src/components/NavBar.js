import React from 'react'
import {Navbar, Nav} from 'react-bootstrap'

// Component for the Navbar of the website
const NavBar = () => {
    return (
      <Navbar bg="dark" variant="dark">
      <Navbar.Brand href="/">Aid All Around</Navbar.Brand>
      <Nav className="mr-auto">
        <Nav.Link href="/">Home</Nav.Link>
        <Nav.Link href="/countries">Countries</Nav.Link>
        <Nav.Link href="/charities">Charities</Nav.Link>
        <Nav.Link href="/articles">Articles</Nav.Link>
        <Nav.Link href="/about">About</Nav.Link>
        <Nav.Link href="/search">Search</Nav.Link>
        <Nav.Link href="/vis">Visualization</Nav.Link>
      </Nav>
    </Navbar>
    )
}

export default NavBar;
