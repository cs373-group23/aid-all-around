import { Card, Button } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import Highlighter from "react-highlight-words";
import { Link } from 'react-router-dom'

const capital_category =
{
    'general': 'General',
    'business': 'Business'
}

const ArticleSearchCard = (props) => {
    const data = props.data
    return (
            <Card hoverable style={{ width: '90%', lineHeight: '95%' }}>

                <Card.Body>
                    
                    <Link to={`/articles/${data.article_id}`} className="link">
                    <Card.Title >
                        <Highlighter
                            highlightClassName="bg-success"
                            searchWords={props.query}
                            autoEscape={true}
                            textToHighlight={data.articleName}
                        />
                    </Card.Title>
                    </Link>
                    <Card.Text>Source: {" "}
                        <Highlighter
                            highlightClassName="bg-success"
                            searchWords={props.query}
                            autoEscape={true}
                            textToHighlight={data.sourceName}
                        />
                    </Card.Text>
                    <Card.Text>Reported Country: {" "}
                        <Highlighter
                            highlightClassName="bg-success"
                            searchWords={props.query}
                            autoEscape={true}
                            textToHighlight={data.reportedCountry}
                        />
                    </Card.Text>
                    <Card.Text>Published At: {" "}
                        <Highlighter
                            highlightClassName="bg-success"
                            searchWords={props.query}
                            autoEscape={true}
                            textToHighlight={data.publishedAt.substring(0, 10)}
                        />
                    </Card.Text>
                    <Card.Text>Source Country: {" "}
                        <Highlighter
                            highlightClassName="bg-success"
                            searchWords={props.query}
                            autoEscape={true}
                            textToHighlight={data.sourceCountry}
                        />
                    </Card.Text>
                    <Card.Text>Category: {" "}
                        <Highlighter
                            highlightClassName="bg-success"
                            searchWords={props.query}
                            autoEscape={true}
                            textToHighlight={data.category.charAt(0).toUpperCase() + data.category.slice(1)}
                        />
                    </Card.Text>
                </Card.Body>
            </Card>
    )
}

export default ArticleSearchCard