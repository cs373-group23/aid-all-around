import { Card, Button } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';

const capital_category = 
    {'general': 'General',
    'business': 'Business'
}

const ArticleCard = (props) => {
    const data = props.data
    return (
        <Card style={{width: '90%', lineHeight: '95%'}}>
            <Card.Img variant="top" src={data.articleImage} style={{
                objectFit: 'cover',
                width: '100%',
                height: '25vh'
            }} />
            
            <Card.Body>
                <Card.Title>{data.articleName}</Card.Title>
                <Card.Text>Source: {data.sourceName}</Card.Text>
                <Card.Text>Reported Country : {data.reportedCountry}</Card.Text>
                <Card.Text>Published At : {data.publishedAt.substring(0, 10)}</Card.Text>
                <Card.Text>Source Country : {data.sourceCountry}</Card.Text>
                <Card.Text>Category : {data.category.charAt(0).toUpperCase() + data.category.slice(1)}</Card.Text>
            </Card.Body>

            <Button href={`/articles/${data.article_id}`} variant="primary">See Details</Button>
        </Card>
    )
}

export default ArticleCard