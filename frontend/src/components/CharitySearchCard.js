import { Card, Button } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import Highlighter from "react-highlight-words";
import { Link } from 'react-router-dom'

const CharitySearchCard = (props) => {
    const data = props.data

    return (
        <Card style={{width: '90%', lineHeight: '95%'}}>

            <Card.Body>
            <Link to={`/charities/${data.charity_id}`} className="link">
                <Card.Title>
                    <Highlighter
                        highlightClassName="bg-success"
                        searchWords={props.query}
                        autoEscape={true}
                        textToHighlight={data.charityName}
                    />    
                </Card.Title>
            </Link>
                <Card.Text>Classification: {" "} <Highlighter
                        highlightClassName="bg-success"
                        searchWords={props.query}
                        autoEscape={true}
                        textToHighlight={data.nteeClassification}
                    />  </Card.Text>
                
                <Card.Text>Income Amount:  {" "} <Highlighter
                        highlightClassName="bg-success"
                        searchWords={props.query}
                        autoEscape={true}
                        textToHighlight={data.incomeAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                    />  </Card.Text>

                <Card.Text>Asset Amount: {" "} <Highlighter
                        highlightClassName="bg-success"
                        searchWords={props.query}
                        autoEscape={true}
                        textToHighlight={data.assetAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                    />  </Card.Text>
                
                <Card.Text>Cause Name: {" "} <Highlighter
                        highlightClassName="bg-success"
                        searchWords={props.query}
                        autoEscape={true}
                        textToHighlight={data.causeName.charAt(0).toUpperCase() + data.causeName.slice(1)}
                    />  </Card.Text>

                <Card.Text>Rating:  {" "} {data.rating} </Card.Text>

            </Card.Body>
        </Card>
    )
}

export default CharitySearchCard