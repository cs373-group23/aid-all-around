import { Container, Row, Col } from 'react-bootstrap'
import { useHistory } from 'react-router-dom';
import React, { useEffect, useState } from 'react';
import { ScatterSeries, Chart, PieSeries, Title, Legend, Tooltip, ArgumentAxis, ValueAxis, BarSeries } from '@devexpress/dx-react-chart-material-ui';
import { Stack, Animation, EventTracker } from '@devexpress/dx-react-chart';
import ResizeableBox from '../../components/ResizeableBox'
import BubbleChart from '../../components/ExpBubbleChart'

const Visualization = () => {
	let history = useHistory();
	const [loading, setLoading] = useState(true);
	const [charityData, setCharityData] = useState([]);
	const [articleData, setArticleData] = useState([]);
	const [countryData, setCountryData] = useState([]);
	const [collegeData, setCollegeData] = useState([]);
	const [countiesData, setCountiesData] = useState([]);
	const [citiesData, setCitiesData] = useState([]);

	const getUrls = (type) => {
		let urls = []
		for (let curPage = 1; curPage < 12; curPage++) {
			urls.push(`https://www.aidallaround.me/api/${type}?page=${curPage}`)
		}
		return urls
	}

	useEffect(() => {
		const fetch_and_set_data = async () => {
			const getData = async () => {
				const requests = [
					`https://www.aidallaround.me/api/charity`,
					...getUrls("article"),
					...getUrls("country"),
					`https://api.relocccate.me/colleges`,
					`https://api.relocccate.me/counties`,
					`https://api.relocccate.me/cities`
				].map((url) => {
					return fetch(url)
						.then((response) => {
							return response.json()
						})
				})
				return Promise.all(requests)
			}

			const apiResponseArr = await getData()

			const charities = apiResponseArr[0]
			let charMap = new Map();
			charities.objects.forEach((element) => {
				if (charMap.get(element["causeName"]) != null) {
					charMap.set(element["causeName"], charMap.get(element["causeName"]) + 1)
				} else {
					charMap.set(element["causeName"], 1)
				}
			});
			setCharityData(Array.from(charMap, ([name, value]) => ({ "causeName": name, "val": value })));

			let artiMap = new Map();
			for (var curPage = 1; curPage < 12; curPage++) {
				const articles = apiResponseArr[curPage]
				articles.objects.forEach((element) => {
					if (artiMap.get(element["sourceName"]) != null) {
						artiMap.set(element["sourceName"], artiMap.get(element["sourceName"]) + 1)
					} else {
						artiMap.set(element["sourceName"], 1)
					}
				});
			}
			setArticleData(Array.from(artiMap, ([name, value]) => ({ "Name": name, "Count": value })));

			let countryMap = new Map();
			for (var curPage = 12; curPage < 23; curPage++) {
				const countries = apiResponseArr[curPage]
				countries.objects.forEach((element) => {
					if (countryMap.get(element["region"]) == null) {
						var counts = [0, 0, 0, 0]
						countryMap.set(element["region"], counts)
					}

					if (element["incomeLevel"] == "Low income") {
						var counts = countryMap.get(element["region"]);
						counts[0] += 1;
						countryMap.set(element["region"], counts)
					} else if (element["incomeLevel"] == "Lower middle income") {
						var counts = countryMap.get(element["region"]);
						counts[1] += 1;
						countryMap.set(element["region"], counts)
					} else if (element["incomeLevel"] == "Upper middle income") {
						var counts = countryMap.get(element["region"]);
						counts[2] += 1;
						countryMap.set(element["region"], counts)
					} else { // High income
						var counts = countryMap.get(element["region"]);
						counts[3] += 1;
						countryMap.set(element["region"], counts)
					}
				});
			}
			setCountryData(Array.from(countryMap, ([key, value]) => ({ "region": key, "low": value[0], 'lowerMiddle': value[1], 'upperMiddle': value[2], 'high': value[3] })));

			const colleges = [apiResponseArr[23].data]
			let collMap = new Map();
			colleges[0].forEach((element) => {
				if (collMap.get(element["state_long"]) != null) {
					collMap.set(element["state_long"], collMap.get(element["state_long"]) + element["total_enroll"])
				} else {
					collMap.set(element["state_long"], element["total_enroll"])
				}
			});
			setCollegeData(Array.from(collMap, ([name, value]) => ({ "Name": name, "Count": value })));

			const counties = [apiResponseArr[24].data]
			counties[0].forEach((element) => {
				countiesData.push({ "temp": element["temp"], "humidity": element["humidity"] })
			});

			const cities = [apiResponseArr[25].data]
			let citiesMap = { "1": 0, "2": 0, "3": 0, "4": 0, "5": 0, "6": 0 }
			cities[0].forEach((element) => {
				var range = Math.floor((element["overall"] - 1) / 10) - 1
				citiesMap[range.toString()] += 1
			});

			citiesData.push(
				{ "Range": "21-30", "Count": citiesMap["1"] },
				{ "Range": "31-40", "Count": citiesMap["2"] },
				{ "Range": "41-50", "Count": citiesMap["3"] },
				{ "Range": "51-60", "Count": citiesMap["4"] },
				{ "Range": "61-70", "Count": citiesMap["5"] },
				{ "Range": "71-80", "Count": citiesMap["6"] },
			)

			setLoading(false)
		};

		fetch_and_set_data()
	}, []);

	return (
		<>
			<Container fluid style={{ margin: "20px" }}>
				<h1>Visualizations</h1><br />

				{!loading ?
					<div>
						<h3>Visualizations for Our Website</h3>
						<h5>Charity Cause Name Visualization</h5>
						<ResizeableBox>
							<Chart
								data={charityData}
								width="1000"
							>
								<PieSeries
									valueField="val"
									argumentField="causeName"
									innerRadius={0.6}
								/>
								<Animation />
								<Legend />
								<EventTracker />
								<Tooltip />
							</Chart>
						</ResizeableBox>

						<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
						<h5>Article Source Visualization</h5>

						<BubbleChart
							graph={{
								zoom: 1.1,
								offsetX: 0,
								offsetY: 0,
							}}
							width={1000}
							height={800}
							padding={0}
							showLegend={true}
							legendPercentage={20}
							legendFont={{
								family: 'Arial',
								size: 12,
								color: '#000',
								weight: 'bold',
							}}
							valueFont={{
								family: 'Arial',
								size: 12,
								color: '#fff',
								weight: 'bold',
							}}
							labelFont={{
								family: 'Arial',
								size: 16,
								color: '#fff',
								weight: 'bold',
							}}
							data={articleData.map(el => {
								return { label: el.Name, value: el.Count }
							})}
						/>





						{/* <BubbleChart data={articleData.map(e => dataForPacking(e))} size={[800, 600]} scale={4} /> */}

						< h5 > Country Income Level Grouped by Region Visualization</h5>
						<Chart
							data={countryData}
						>
							<ArgumentAxis />
							<ValueAxis />

							<BarSeries
								name="Low income"
								valueField="low"
								argumentField="region"
								color="#c0c0c0"
							/>
							<BarSeries
								name="Lower middle income"
								valueField="lowerMiddle"
								argumentField="region"
								color="#ffd700"
							/>
							<BarSeries
								name="Upper middle income"
								valueField="upperMiddle"
								argumentField="region"
								color="#ffa31a"
							/>
							<BarSeries
								name="High income"
								valueField="high"
								argumentField="region"
								color="#cd7f32"
							/>
							<Animation />
							<Legend />
							<EventTracker />
							<Tooltip />
							<Stack />
						</Chart>

						<br /><br />
						<h3>Visualizations for <a href="https://www.relocccate.me/">Our Provider's Website</a></h3>
						<h5>College Total Enrollment by State Visualization</h5>

						<BubbleChart
							graph={{
								zoom: 1.0,
								offsetX: 0,
								offsetY: 0,
							}}
							width={1200}
							height={1000}
							padding={0}
							showLegend={true}
							legendPercentage={20}
							legendFont={{
								family: 'Arial',
								size: 12,
								color: '#000',
								weight: 'bold',
							}}
							valueFont={{
								family: 'Arial',
								size: 12,
								color: '#fff',
								weight: 'bold',
							}}
							labelFont={{
								family: 'Arial',
								size: 16,
								color: '#fff',
								weight: 'bold',
							}}
							data={collegeData.map(el => {
								return { label: el.Name, value: el.Count }
							})}
						/>

						{/* <BubbleChart data={collegeData.map(e => dataForPacking(e))} size={[800, 600]} scale={0.0002} /> */}

						<h5>Temperature/Humidity Visualization</h5>
						<p>(x: Humidity | y: Temperature)</p>
						<Chart
							data={countiesData}
							width="1000"
						>
							<ArgumentAxis showGrid />
							<ValueAxis />
							<ScatterSeries
								valueField="temp"
								argumentField="humidity"
							/>
							<Animation />
							<EventTracker />
							<Tooltip />
						</Chart>

						<br />
						<h5>Overall Safety Scores Range Visualization</h5>
						<p>(x: Overall safety score ranges | y: Number of cities in this range)</p>
						<Chart
							data={citiesData}
						>
							<ArgumentAxis />
							<ValueAxis max={6} />

							<BarSeries
								valueField="Count"
								argumentField="Range"
							/>
							<Animation />
							<EventTracker />
							<Tooltip />
						</Chart>
					</div>
					: <Container fluid style={{ margin: "20px" }}><p>Loading......(it may take a few seconds)</p></Container>}
			</Container>
		</>
	);
}

export default Visualization;
