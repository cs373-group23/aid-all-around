import CountryCard from '../../components/CountryCard'
import { Container, Row, Col } from 'react-bootstrap'
import { useEffect, useState } from 'react';
import Paginate from '../../components/Paginate'
import CountrySort from '../../components/Sort'
import CountryFilter from '../../components/Filter'
import CountryNumFilter from '../../components/CountryNumFilter'
import SearchBar from '../../components/SearchBar'
 
const Countries = () => {
  const [countries, setCountries] = useState([]);
  const [loading, setLoading] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalCountries, setTotalCountries] = useState(0);
  const [pagelimit, setPageLimit] = useState(0)
  const [incomeFilters, setIncomeFilters] = useState([])
  const [regionFilters, setRegionFilters] = useState([])
  const [latitudeFilters, setLatitudeFilters] = useState([])
  const [longitudeFilters, setLongitudeFilters] = useState([])
  const [queries, setQueries] = useState([])

  useEffect(() => {
    const fetch_and_set_countries = async () => {
      let fetchUrl = `https://www.aidallaround.me/api/country?page=${currentPage}&q={"order_by":${JSON.stringify(queries)}, "filters":[{"and": [{"or": ${JSON.stringify(incomeFilters)}}, {"or": ${JSON.stringify(regionFilters)}}, {"or": ${JSON.stringify(latitudeFilters)}}, {"or": ${JSON.stringify(longitudeFilters)}}]}]}`
      const response = await fetch(fetchUrl)
      const countries = await response.json()
      setCountries(countries.objects)
      setTotalCountries(countries.num_results)
      setPageLimit(Math.ceil(countries.num_results / 10))
      setLoading(false)
    }

    // Gets and sets countries from the backend api
    fetch_and_set_countries()
  }, [currentPage, queries, incomeFilters, regionFilters, latitudeFilters, longitudeFilters]);

  return (
    <>
      { !loading ? (
        <Container fluid style={{ margin: "20px" }}>
          
          <div className="row" style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            <Paginate page={currentPage} limit={pagelimit} setPage={setCurrentPage} />
          </div>
          <div className="row" style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            <p> Showing Page {currentPage} of {pagelimit} </p>
          </div>
          
          <h3>Countries | Total {totalCountries} Countries </h3>
          <SearchBar modelString="country"/>
          <Row sm={3}>
            <Col>
              <CountrySort queries={queries} setQueries={setQueries} options = {["population", "latitude", "longitude", "gini"]}/>
            </Col>
            <Col>
              <CountryFilter filters={incomeFilters} setFilters={setIncomeFilters} name={"incomeLevel"} options={["Low income", "Lower middle income", "Upper middle income", "High income"]}/>
            </Col>
            <Col>
              <CountryFilter filters={regionFilters} setFilters={setRegionFilters} name={"region"} options={["Asia", "Oceania", "Europe", "Americas", "Africa"]}/>
            </Col>
            <Col>
              <CountryNumFilter filters={latitudeFilters} setFilters={setLatitudeFilters} name={"latitude"} options={["-90.0 -60.0", "-60.0 -30.0", "-30.0 0.0", "0.0 30.0", "30.0 60.0", "60.0 90.0"]}/>
            </Col>
            <Col>
              <CountryNumFilter filters={longitudeFilters} setFilters={setLongitudeFilters} name={"longitude"} options={["-180.0 -120.0", "-120.0 -60.0", "-60.0 0.0", "0.0 60.0", "60.0 120.0", "120.0 180.0"]}/>
            </Col>
          </Row>
          <br />

          <Row noGutters sm={5}>
            {countries.map((country) => {
              return (
                <Col>
                  <CountryCard data={country} />
                </Col>
              )
            })
            }
          </Row>

          <br />
          <div className="row" style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            <Paginate page={currentPage} limit={pagelimit} setPage={setCurrentPage} />
          </div>
          <div className="row" style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            <p> Showing Page {currentPage} of {pagelimit} </p>
          </div>
          
        </Container>
      ) : undefined
      }
    </>
  );
}

export default Countries;