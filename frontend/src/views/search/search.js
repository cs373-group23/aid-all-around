import CountryCard from '../../components/CountryCard'
import ArticleCard from '../../components/ArticleCard'
import CharityCard from '../../components/CharityCard'
import { Form, FormControl, Button, Container, Row, Col } from 'react-bootstrap'
import { useEffect, useState } from 'react';
import CountrySort from '../../components/Sort'
import CountryFilter from '../../components/Filter'
import queryString from 'query-string'
import { Link, useHistory } from 'react-router-dom'
import ArticleSearchCard from '../../components/ArticleSearchCard'
import CharitySearchCard from '../../components/CharitySearchCard'
import CountrySearchCard from '../../components/CountrySearchCard'
import Scrollspy from 'react-scrollspy'

const Search = (props) => {
  const initialSearchTerms = props.location.search ? queryString.parse(props.location.search).q : ""
  const searchModel = props.location.search ? queryString.parse(props.location.search).b : ""
  
  const [searchTerms, setSearchTerms] = useState(initialSearchTerms);
  const [renderSearchTerms, setRenderSearchTerms] = useState(initialSearchTerms.split(" "))
  const [loading, setLoading] = useState(true);
  const [searchCountry, setSearchCountry] = useState(true);
  const [searchCharity, setSearchCharity] = useState(true);
  const [searchArticle, setSearchArticle] = useState(true);

  // Countries
  const [countries, setCountries] = useState([]);
  const [countryFilters, setCountryFilters] = useState([])

  // Articles
  const [articles, setArticles] = useState([]);
  const [articleFilters, setArticleFilters] = useState([])

  // Charities
  const [charities, setCharities] = useState([]);
  const [charityFilters, setCharityFilters] = useState([])

  let countryList = ['name', 'incomeLevel', 'region', 'capital', 'currencies', 'languages', 'alpha2Code']
  let charityList = ['charityName', 'nteeClassification', 'causeName', 'ein', 'mission', 'nteeType', 'tagLine', 'websiteURL']
  let articleList = ['articleName', 'sourceName', 'reportedCountry', 'publishedAt', 'sourceCountry', 'category', 'language', 'websiteURL', 'description', 'articleAuthor', 'articleSnippet']

  let filtersList = [countryFilters, charityFilters, articleFilters]
  let attributeList = [countryList, charityList, articleList]

  useEffect(() => {
    setCountryFilters([])
    setCharityFilters([])
    setArticleFilters([])

    for (let i = 0; i < attributeList.length; i++) {
      let filter = filtersList[i]
      let attribs = attributeList[i]
      for (const attrib of attribs) {
        for (const term of renderSearchTerms) {
          filter.push({ "name": attrib, "op": "ilike", "val": "%" + term + "%" })
        }
      }
    }

    if (searchModel == "country") {
      setSearchCharity(false)
      setSearchArticle(false)
    }
    
    if (searchModel == "charity") {
      setSearchCountry(false)
      setSearchArticle(false)
    }
    
    if (searchModel == "article") {
      setSearchCountry(false)
      setSearchCharity(false)
    }

    if (searchModel == "splash" || searchModel == "search") {
      setSearchCountry(true)
      setSearchCharity(true)
      setSearchArticle(true)
    }

    const fetch_and_set = async () => {
      let fetchCountryUrl = `https://www.aidallaround.me/api/country?page=1&q={"filters":[{"or":${JSON.stringify(countryFilters)}}]}`
      const countryResponse = await fetch(fetchCountryUrl)
      const countries = await countryResponse.json()
      setCountries(countries.objects)

      let fetchArticleUrl = `https://www.aidallaround.me/api/article?page=1&q={"filters":[{"or":${JSON.stringify(articleFilters)}}]}`
      const articleReponse = await fetch(fetchArticleUrl)
      const articles = await articleReponse.json()
      setArticles(articles.objects)

      let fetchCharityUrl = `https://www.aidallaround.me/api/charity?page=1&q={"filters":[{"or":${JSON.stringify(charityFilters)}}]}`
      const charityReponse = await fetch(fetchCharityUrl)
      const charities = await charityReponse.json()

      setCharities(charities.objects)
      setLoading(false)
    }

    // Gets and sets data from the backend api
    if (initialSearchTerms.length) {
      fetch_and_set()
    }
  }, [renderSearchTerms]);

  let history = useHistory();

  return (
    <>
      <Container fluid style={{ margin: "20px" }}>
        <h3>Search </h3>
        <Form>
          <Form.Row className="align-items-center">
            <Col xs="7">
              <Form.Label htmlFor="inlineFormInput" srOnly>
                Search
              </Form.Label>
              <Form.Control
                className="mb-2"
                id="inlineFormInput"
                placeholder="Search"

                onChange={(e) => {
                  setSearchTerms(e.target.value)
                }}
              />
            </Col>

            <Col>
              <Button onClick={() => {
                setRenderSearchTerms(searchTerms.split(" "))
                history.push(`search?q=${searchTerms}&b=search`)
              }
              }> Search </Button>
            </Col>
          </Form.Row>
        </Form>

      </Container>
        { (!loading) && searchCountry ? (
          <Container fluid style={{ margin: "20px" }}>
            <h3>Countries</h3>
            <Row noGutters sm={5}>
              {countries && countries.map((country) => {
                return (
                  <Col>
                    <CountrySearchCard data={country} query = {renderSearchTerms} />
                  </Col>
                )
              })
              }
            </Row>
          </Container>
        ) : undefined
        }

        { (!loading) && searchCharity ? (
          <Container fluid style={{ margin: "20px" }}>
            <h3>Charities</h3>
              <Row noGutters sm={5}>
                {charities && charities.map((charity) => {
                  return (
                    <Col>
                      <CharitySearchCard data={charity} query = {renderSearchTerms} />
                    </Col>
                  )
                })
                }
              </Row>
          </Container>
          ) : undefined
        }

        { (!loading) && searchArticle ? (
          <Container fluid style={{ margin: "20px" }}>
            <h3>Articles</h3>
              <Row noGutters sm={5}>
                {articles && articles.map((article) => {
                  return (
                    <Col>
                      <ArticleSearchCard data={article} query = {renderSearchTerms} />
                    </Col>
                  )
                })
                }
              </Row>
          </Container>
          ) : undefined
        }
    </>
  );
}

export default Search;