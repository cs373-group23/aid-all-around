import { useEffect, useState } from 'react';
import TeamCard from '../../components/TeamCard'
import { Container, Row, Col } from 'react-bootstrap'
import YouTube from 'react-youtube';

const issues_url = "https://gitlab.com/api/v4/projects/cs373-group23%2Faid-all-around/issues?per_page=1000"
const commits_url = "https://gitlab.com/api/v4/projects/cs373-group23%2Faid-all-around/repository/commits?ref_name=master&per_page=1000"

const About = () => {

    const [numCommits, setNumCommits] = useState("")
    const [numIssues, setNumIssues] = useState("")
    const [issuesByPerson, setissuesByPerson] = useState(new Map())
    const [commitsByPerson, setcommitsByPerson] = useState(new Map())

    const opts = {
        height: '390',
        width: '640',
        playerVars: {
          autoplay: 0,
        },
      };

    useEffect(() => {
        const fetch_and_set_data = async () => {
            const issue_response = await fetch(issues_url)
            const issue_data = await issue_response.json()

            var issues = new Map();

            issue_data.forEach((element) => {
                var gitName = element.author.name

                if (issues.get(gitName) != null) {
                    issues.set(gitName, issues.get(gitName) + 1)
                } else {
                    issues.set(gitName, 1)
                }
            });

            setissuesByPerson(issues);

            setNumIssues(issue_data.length)

            const commit_response = await fetch(commits_url)
            const commit_data = await commit_response.json()

            var commits = new Map();

            commit_data.forEach((element) => {
                var gitName = element.author_name

                if (commits.get(gitName) != null) {
                    commits.set(gitName, commits.get(gitName) + 1)
                } else {
                    commits.set(gitName, 1)
                }
            });

            setcommitsByPerson(commits);

            setNumCommits(commit_data.length)
        }
        fetch_and_set_data()
    }, [])

    return (
        <div>
            <Container>
                <Container>
                    <br />
                    <h1>Aid All Around</h1>
                </Container>

                <Container>
                    <p>Purpose: To gather and make available information about countries, charities, and news worldwide.</p>
                    <p>Description: A website to encourage people to be informed about the state of the world today, and that provides an avenue for supporting different places and causes.</p><br />
                    <p>Intended Users: Those looking to educate themselves on issues worldwide and seek a way to help alleviate them. </p>
                    <p>Explanation of the Interesting Result of Integrating Disparate Data: It is enlightening to see patterns between the issues that affect a country, the issues that charities aiding that country focus on, and the issues that the news sources that report on that country cover.</p><br />
                    <p>Description of Data Scraping: We scraped all the information for our database by using a python program to make API calls for the data in bulk, looping through the data to create instances of our database models, then pushing all the instances onto our database. </p><br />
                    
                    <YouTube videoId="4me75AiGZu8" opts={opts} /><br />
                    
                    <p>Data APIs:</p>
                    <a href="https://charity.3scale.net/docs/data-api/reference">Data for charity</a><br />
                    <a href="https://datahelpdesk.worldbank.org/knowledgebase/articles/898590-country-api-queries">Data for income level of countries</a><br />
                    <a href="https://restcountries.eu/">Data for countries</a><br />
                    <a href="https://newsapi.org/docs">Data for news sources</a><br /><br />
                    <p>Tools:</p>
                    <p>Docker: used to maintain a consistent development environment, along with providing an environment for the deployment of the backend on AWS</p>
                    <p>GitLab: used for organizing (issues and user stories), along with storing/updating source code</p>
                    <p>React: used for frontend development (method being JavaScript)</p>
                    <p>Bootstrap: CSS framework</p>
                    <p>AWS: used for hosting the website (both frontend and backend)</p>
                    <p>Postman: used to design and document the API, along with creating the unit tests for it</p>
                    <p>Flask: used for backend development (method being Python)</p>
                    <p>Selenium: used for creating GUI unit tests</p>
                    <p>Jest: used for creating JavaScript code unit tests</p>
                    <p>pgAdmin: used to inspect and modify our PostgreSQL database</p>
                    <p>PlantUML: used to create a UML diagram of our models</p>
                    <p>DBDiagram: used to create a DB diagram of our models</p>
                    <p>D3: used for data visualization</p>
                    <a href="https://gitlab.com/cs373-group23/aid-all-around">Gitlab Repo Link</a><br />
                    <a href="https://documenter.getpostman.com/view/14749271/TzJpj1DR">Postman API Link</a><br />
                    <br /><br />
                </Container>
                <Row>
                    <Col sm={4}>
                        <h3>Total Commits : {numCommits}</h3>
                    </Col>
                    <Col sm={4}>
                        <h3>Total Issues : {numIssues}</h3>
                        <p>Issues from our customer: {numIssues - issuesByPerson.get("Ilham Thomson") - issuesByPerson.get("Shivang Singh") - issuesByPerson.get("Connor Thompson") - issuesByPerson.get("Abdul Maanda") - issuesByPerson.get("Zhiyao Bao")}</p>
                    </Col>
                    <Col sm={4}>
                        <h3>Total Unit Tests: 58</h3>
                        <p>Postman: 12</p>
                        <p>Backend: 18</p>
                        <p>Frontend: 17</p>
                        <p>GUI: 11</p>
                    </Col>
                </Row>
            </Container>
            <br />

            <Container>
                <Row>
                    <Col sm={4}>
                        <TeamCard
                            name="Ilham Thomson"
                            src="https://media-exp1.licdn.com/dms/image/C4E03AQHHT18r2nBTLg/profile-displayphoto-shrink_200_200/0/1577720823968?e=1620259200&v=beta&t=V3omYYradbcR5Fbuwtt9iuSjA_a4WjBzp5OXmJ8Lwh4"
                            bio="I'm a Junior in CS at the University of Texas at Austin who is interested in backend web development. In my free time, I like to read and take walks."
                            responsibilities="Backend & Frontend"
                            commits={commitsByPerson.get("Ilham Thomson") + commitsByPerson.get("ThorTheDwarf")}
                            issues={issuesByPerson.get("Ilham Thomson")}
                            tests={15}
                        />
                    </Col>
                    <Col sm={4}>
                        <TeamCard
                            name="Shivang Singh"
                            src="https://media-exp1.licdn.com/dms/image/C4E03AQHxx9DXeqAPvg/profile-displayphoto-shrink_800_800/0/1589499133321?e=1620259200&v=beta&t=omxRQj2C1sEBJTgNfjVsyZNvXoK4STit0rsxUkL-FfU"
                            bio="I am a Senior in CS pursuing an Integrated 5 year Masters at the University of Texas at Austin. My interests include Natural Language Processing (NLP) research and software engineering."
                            responsibilities="Backend & Frontend"
                            commits={commitsByPerson.get("Shivang Singh")}
                            issues={issuesByPerson.get("Shivang Singh")}
                            tests = {15}
                        />
                    </Col>
                    <Col sm={4}>
                        <TeamCard
                            name="Thomas Connor Thompson"
                            src="https://imgur.com/q6wGLLw.jpg"
                            bio="I am a Senior in CS at the University of Texas at Austin interested in data science, graphics, and how technology has changed and affects our world. In my free time I enjoy reading stories, playing DnD, and hiking/camping with friends."
                            responsibilities="Frontend (Dropped Class After Phase 1)"
                            commits={commitsByPerson.get("thocotho")}
                            issues={issuesByPerson.get("Connor Thompson")}
                            tests = {0}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col sm={4}>
                        <TeamCard
                            name="Abdul Rehman Maanda"
                            src="https://miro.medium.com/max/700/1*-Lu5uQKkO3RU73EWJkqUFQ.jpeg"
                            bio="I am a Senior in CS at the University of Texas at Austin. My areas of interest include reverse engineering, internet security and ethical hacking."
                            responsibilities="Backend & Frontend"
                            commits={commitsByPerson.get("Abdul Maanda") + commitsByPerson.get("themaanda")}
                            issues={issuesByPerson.get("Abdul Maanda")}
                            tests = {14}
                        />
                    </Col>
                    <Col sm={4}>
                        <TeamCard
                            name="Zhiyao Bao"
                            src="https://media.licdn.cn/dms/image/C5603AQFyfTUoBH8x2g/profile-displayphoto-shrink_400_400/0/1598817756877?e=1620259200&v=beta&t=0Jev5i96Gmmk_kLgB43LOt3iu-82zLAyM5CmerK6p-A"
                            bio="I am a Junior in CS at the University of Texas at Austin. I'm interested in machine learning and software engineering. I'm passionate about technology and exploring new things. In my free time, I love watching movies and playing outdoors sports."
                            responsibilities="Backend & Frontend"
                            commits={commitsByPerson.get("Zhiyao Bao") + commitsByPerson.get("zhiyao") + commitsByPerson.get("zhiyao-bz")}
                            issues={issuesByPerson.get("Zhiyao Bao")}
                            tests = {14}
                        />
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default About;
