import { forwardRef } from 'react';
import MaterialTable from 'material-table';
import { Container, Row, Col, Pagination } from 'react-bootstrap'
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import CharityFilter from '../../components/Filter'
import SearchBar from '../../components/SearchBar'
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';

const Charities = () => {
    let history = useHistory();

    const [charityData, setCharityData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [totalCharities, setTotalCharities] = useState(0);
    const [classFilters, setClassFilters] = useState([])
    const [causeFilters, setCauseFilters] = useState([])
    const [ratingFilters, setRatingFilters] = useState([])

    const tableIcons = {
        Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
        Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
        Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
        Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
        DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
        Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
        Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
        Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
        FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
        LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
        NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
        PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
        ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
        Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
        SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
        ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
        ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
    };

    useEffect(() => {
        const fetch_and_set_charities = async () => {
            const response = await fetch(`https://www.aidallaround.me/api/charity?q={"filters":[{"and": [{"or": ${JSON.stringify(classFilters)}}, {"or": ${JSON.stringify(causeFilters)}}, {"or": ${JSON.stringify(ratingFilters)}}]}]}`)
            const charities = await response.json()
            setTotalCharities(charities.num_results)
            setCharityData(charities.objects)
            setLoading(false)
        }
        // Gets and sets countries from the backend api
        fetch_and_set_charities()
    }, [classFilters, causeFilters, ratingFilters]);

    const keys = [
        { title: "id", field: "charity_id", width: "4%" },
        { title: "Name", field: "charityName" ,searchable: true},
        { title: "Classification", field: "nteeClassification",searchable: true },
        { title: "Income Amount", field: "incomeAmount", searchable: true },
        { title: "Asset Amount", field: "assetAmount" , searchable: true},
        { title: "Cause Name", field: "causeName", searchable: true },
        { title: "Rating (out of 4)", field: "rating" ,searchable: true},
    ]

    return (
        <div >
            <Container fluid style={{ margin: "20px" }}>
                <h3>Charities | Total {totalCharities} Charities | Total {Math.ceil(totalCharities / 10)} Pages</h3>
                <SearchBar modelString="charity"/>
                <Row sm={3}>
                    <Col>
                        <CharityFilter filters={classFilters} setFilters={setClassFilters} name={"nteeClassification"} options={["Residential, Custodial Care","Natural Resources Conservation and Protection","International Relief","International Student Exchange and Aid","Cancer Research","Biological, Life Science Research","Named Trusts/Foundations N.E.C. ","Christian","Food Service, Free Food Distribution Programs","Cultural, Ethnic Awareness","Minority Rights",,"Nerve, Muscle, Bone Research","Promotion of International Understanding","Youth Development N.E.C.","Children's, Youth Services","International Development, Relief Services","Education N.E.C.","Religion Related, Spiritual Development N.E.C.","Alliance/Advocacy Organizations ","Private Grantmaking Foundations","Neighborhood Centers, Settlement Houses","Human Service Organizations - Multipurpose","International Economic Development","Disaster Preparedness and Relief Services","International Human Rights","International Exchanges, N.E.C.","Housing Support Services -- Other","Public Foundations","Services to Promote the Independence of Specific Populations","Remedial Reading, Reading Encouragement","Research Institutes and/or Public Policy Analysis ","Fund Raising and/or Fund Distribution"]} />
                    </Col>
                    <Col>
                        <CharityFilter filters={causeFilters} setFilters={setCauseFilters} name={"causeName"} options={["International Peace, Security, and Affairs","Wildlife Conservation","Environmental Protection and Conservation","Youth Development, Shelter, and Crisis Services","Scholarship and Financial Support","Development and Relief Services","Advocacy and Education","Humanitarian Relief Supplies","Adult Education Programs and Services","Animal Rights, Welfare, and Services","Youth Education Programs and Services","Treatment and Prevention Services","Diseases, Disorders, and Disciplines","Religious Media and Broadcasting","Religious Activities","Non-Medical Science & Technology Research"]} />
                    </Col>
                    <Col>
                        <CharityFilter filters={ratingFilters} setFilters={setRatingFilters} name={"rating"} options={["1", "2", "3", "4"]} />
                    </Col>
                </Row>
            </Container>
						
            {!loading ?
                <MaterialTable
                    data={charityData}
                    columns={keys}
                    options={{
                        showTitle: false,
                        pageSize: 10,
                    }}

                    icons={tableIcons}
                    
                    onRowClick={(event, rowData) => {
                        history.push("/charities/" + rowData.charity_id);
                    }}
                />

                : <Container fluid style={{ margin: "20px" }}><p>Loading......(it may take a few seconds)</p></Container>
            }
        </div>
    );
}

export default Charities;