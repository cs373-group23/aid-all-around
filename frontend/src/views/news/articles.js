import ArticleCard from '../../components/ArticleCard'
import { Container, Row, Col, Pagination } from 'react-bootstrap'
import { useEffect, useState } from 'react';
import Paginate from '../../components/Paginate'
import ArticleSort from '../../components/Sort'
import ArticleFilter from '../../components/Filter'
import SearchBar from '../../components/SearchBar'

const Articles = () => {
  const [articles, setArticles] = useState([]);
  const [loading, setLoading] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalArticles, setTotalArticles] = useState(0);
  const [pagelimit, setPageLimit] = useState(0)
  const [categoryFilters, setCategoryFilters] = useState([])
  const [countryFilters, setCountryFilters] = useState([])
  const [articleFilters, setArticleFilters] = useState([])
  const [queries, setQueries] = useState([])

  useEffect(() => {
    const fetch_and_set_articles = async () => {
      const response = await fetch(`https://www.aidallaround.me/api/article?page=${currentPage}&q={"order_by":${JSON.stringify(queries)}, "filters":[{"and": [{"or": ${JSON.stringify(categoryFilters)}}, {"or": ${JSON.stringify(countryFilters)}}, {"or": ${JSON.stringify(articleFilters)}}]}]}`)
      const articles = await response.json()
      setArticles(articles.objects)
      setTotalArticles(articles.num_results)
      setPageLimit(Math.ceil(articles.num_results / 10))
      setLoading(false)
    }

    // Gets and sets articles from the backend api
    fetch_and_set_articles()
  }, [currentPage, queries, categoryFilters, countryFilters, articleFilters]);

  return (
    <>
      { !loading ? (
        <Container fluid style={{ margin: "20px" }}>

          <div className="row" style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            <Paginate page={currentPage} limit={pagelimit} setPage={setCurrentPage} />
          </div>
          <div className="row" style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            <p> Showing Page {currentPage} of {Math.ceil(totalArticles / 10)} </p>
          </div>
          
          <h3>Articles | Total {totalArticles} Articles </h3>
          <SearchBar modelString="article"/>
          <Row sm={4}>
            <Col>
              <ArticleSort queries={queries} setQueries={setQueries} options={["reportedCountry", "publishedAt"]} />
            </Col>
            <Col>
              <ArticleFilter filters={categoryFilters} setFilters={setCategoryFilters} name={"category"} options={["general", "business"]} />
            </Col>
            <Col>
              <ArticleFilter filters={countryFilters} setFilters={setCountryFilters} name={"sourceCountry"} options={["Great Britain", "United States"]} />
            </Col>
            <Col>
              <ArticleFilter filters={articleFilters} setFilters={setArticleFilters} name={"sourceName"} options={["Time", "The Wall Street Journal", "Reuters", "Google News", "CNN", "BBC News"]} />
            </Col>
          </Row>
          <br />

          <Row noGutters sm={5}>
            {articles.map((article) => {
              return (
                <Col>
                  <ArticleCard data={article} />
                </Col>
              )
            })
            }
          </Row>

          <br />
          <div className="row" style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            <Paginate page={currentPage} limit={pagelimit} setPage={setCurrentPage} />
          </div>
          <div className="row" style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            <p> Showing Page {currentPage} of {Math.ceil(totalArticles / 10)} </p>
          </div>

        </Container>
      ) : undefined
      }
    </>
  );
}

export default Articles;