import { Form, FormControl, Button, Row, Col, Container } from 'react-bootstrap'
import React, { useState } from 'react'
import {Link} from 'react-router-dom'

const backgroundStyle = {
  backgroundPosition: 'center',
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
  width: '100vw',
  height: '100vh',
  backgroundImage: `url("https://natureconservancy-h.assetsadobe.com/is/image/content/dam/tnc/nature/en/photos/Sci2050_BlackMarble_Cropped.png?crop=0,251,4341,2387&wid=2000&hei=1100&scl=2.1705")`,
}

let font_family = 'Georgia'

const Splash = () => {

  const [queryString, setQueryString] = useState([])

  return (
    <div className="background" style={backgroundStyle} >
      <div className="page-container" style={{ display: 'block', alignItems: 'center', justifyContent: 'center' }}>
        <br /><br /><br />
        <div className="row" style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <h1 style={{ color: 'white', fontFamily: font_family }}>Welcome to Aid All Around!</h1>
        </div>
        <div className="row" style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <p style={{ color: 'white', fontFamily: font_family }}>A website to encourage people to be more informed about the state of the world today, and that provides an avenue for supporting different places and causes.</p>
        </div>
        <br /><br /><br />

        <div className="page-container" style={{width: '80%', marginRight: 'auto', marginLeft:'auto'}}>
        <Form>
          <Form.Row className="align-items-center" id="splashSearch">
            <Col xs="10">
              <Form.Label htmlFor="inlineFormInput" srOnly>
                Search
              </Form.Label>
              <Form.Control
                className="mb-2"
                id="inlineFormInput"
                placeholder="Search"
                onChange={(e) => {
                  setQueryString(e.target.value)
                }}
              />
            </Col>
            <Col xs="auto">
            <Link to={`/search?q=${queryString}&b=${'splash'}`} className="btn btn-primary" id="button">Search</Link>
            </Col>
          </Form.Row>
        </Form>

        </div>

        <br />
        <div className="row" style={{ width: '102%', display: 'block', opacity: '50%' }}>
          <ul class="list-group list-group-flush">
            <li class="list-group-item" style={{ color: 'grey', fontSize: '20px', fontFamily: font_family, fontWeight: 'bold', textAlign: 'center' }}><a href='/countries' style={{ color: 'grey' }}>Countries</a></li>
            <li class="list-group-item" style={{ color: 'grey', fontSize: '20px', fontFamily: font_family, fontWeight: 'bold', textAlign: 'center' }}><a href='/charities' style={{ color: 'grey' }}>Charities</a></li>
            <li class="list-group-item" style={{ color: 'grey', fontSize: '20px', fontFamily: font_family, fontWeight: 'bold', textAlign: 'center' }}><a href='/articles' style={{ color: 'grey' }}>Articles</a></li>
          </ul>
        </div>
      </div>
    </div>
  )
}

export default Splash