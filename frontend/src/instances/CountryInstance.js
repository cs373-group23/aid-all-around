import { useState, useEffect } from 'react';
import { Card, Container, Row, Col, Button, OverlayTrigger, Tooltip } from 'react-bootstrap'
import Map from '../components/Map'

const CountryInstance = (props) => {
    let id = props.match.params.id

    const [country, setCountry] = useState([])
    const [loading, setLoading] = useState(true)
    const [articleName, setArticleName] = useState(true)
    const [charityName, setCharityName] = useState(true)

    const renderCharity = (props) => (
        <Tooltip id="button-tooltip" {...props}>
            {charityName}
        </Tooltip>
    );

    const renderArticle = (props) => (
        <Tooltip id="button-tooltip" {...props}>
            {articleName}
        </Tooltip>
    );

    useEffect(() => {
        const fetch_and_set = async () => {
            let response = await fetch(`https://www.aidallaround.me/api/country/${id}`)
            let country = await response.json()
            setCountry(country)

            response = await fetch(`https://www.aidallaround.me/api/charity/${country.country_charity_id}`)
            let charity = await response.json()
            setCharityName(charity.charityName)

            response = await fetch(`https://www.aidallaround.me/api/article/${country.country_article_id}`)
            let article = await response.json()
            setArticleName(article.articleName)

            setLoading(false)
        }
        fetch_and_set()
    }, [])

    var lat = country.latitude + "°N"
    if (country.latitude < 0) {
        lat = -country.latitude + "°S"
    }
    var long = country.longitude + "°E"
    if (country.longitude < 0) {
        long = -country.longitude + "°W"
    }

    return (
        <Container fluid style={{
            marginTop: "20px", display: "flex", justifyContent: "center",
            alignItems: "center",
            textAlign: "center"
        }}>
            <Row noGutters>
                {!loading ?
                    (
                        <>
                            <Col>
                                <Card style={{ width: '50vw' }}>
                                    <Map latitude={country.latlng[0]}
                                        longitude={country.latlng[1]}
                                        zoom={5} />
                                    <Card.Body>
                                        <Card.Title as="h5">{country.name}</Card.Title>
                                        <Card.Img src={country.flag} style={{ width: "200px", height: "120px" }} />
                                        <br /><br />
                                        <Card.Text>Capital : {country.capital}</Card.Text>
                                        <Card.Text>Population : {country.population.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Card.Text>
                                        <Card.Text>Gini Index : {country.gini}</Card.Text>
                                        <Card.Text>Region : {country.region}</Card.Text>
                                        <Card.Text>Lat/Long : {lat + `, ` + long}</Card.Text>
                                        <Card.Text>Main Currency : {country.currencies}</Card.Text>
                                        <Card.Text>Languages : {country.languages}</Card.Text>
                                        <Card.Text>Income Level : {country.incomeLevel}</Card.Text>
                                        <Card.Text>Area (km^2) : {country.area}</Card.Text>
                                        <Card.Text>Alpha2 Code : {country.alpha2Code}</Card.Text>

                                        <OverlayTrigger
                                            placement="top"
                                            delay={{ show: 250, hide: 400 }}
                                            overlay={renderCharity}
                                        >
                                                <Button class="btn btn-secondary" href={`/charities/${country.country_charity_id}`} variant="secondary">See Related Charity</Button>
                                        </OverlayTrigger> {' '}

                                        <OverlayTrigger
                                            placement="top"
                                            delay={{ show: 250, hide: 400 }}
                                            overlay={renderArticle}
                                        >
                                            <Button class="btn btn-secondary" href={`/articles/${country.country_article_id}`} variant="secondary">See Related Article</Button>
                                        </OverlayTrigger> {' '}

                                        <Button href={`/countries`} variant="secondary">All Countries</Button>{' '}
                                    </Card.Body>
                                </Card>
                            </Col>
                        </>
                    ): undefined
                }
            </Row>
        </Container>
    )
}

export default CountryInstance