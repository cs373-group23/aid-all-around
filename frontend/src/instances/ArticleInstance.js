import { useState, useEffect } from 'react';
import { Card, Container, Row, Col, Button, OverlayTrigger, Tooltip } from 'react-bootstrap'
import UrlPreview from '../components/UrlPreview'

const ArticleInstance = (props) => {
    let id = props.match.params.id

    const [article, setArticle] = useState([])
    const [loading, setLoading] = useState(true)
    const [countryName, setCountryName] = useState(true)
    const [charityName, setCharityName] = useState(true)

    const renderCountry = (props) => (
        <Tooltip id="button-tooltip" {...props}>
            {countryName}
        </Tooltip>
    );

    const renderCharity = (props) => (
        <Tooltip id="button-tooltip" {...props}>
            {charityName}
        </Tooltip>
    );

    useEffect(() => {
        const fetch_and_set = async () => {
            let response = await fetch(`https://www.aidallaround.me/api/article/${id}`)
            let article = await response.json()
            setArticle(article)

            response = await fetch(`https://www.aidallaround.me/api/country/${article.article_country_id}`)
            let country = await response.json()
            setCountryName(country.name)

            response = await fetch(`https://www.aidallaround.me/api/charity/${article.article_charity_id}`)
            let charity = await response.json()
            setCharityName(charity.charityName)

            setLoading(false)
        }
        fetch_and_set()
    }, [])

    return (
        <Container fluid style={{
            marginTop: "20px", display: "flex", justifyContent: "center",
            alignItems: "center",
            textAlign: "center"
        }}>
            <Row noGutters>
                {!loading ?
                    (
                        <>
                            <Col>
                                <Card style={{ width: '50vw' }}>
                                    <Card.Body>
                                        <Card.Img src={article.articleImage} style={{ width: "400px", height: "100%" }} />
                                        <br /><br />
                                        <Card.Title as="h5">{article.articleName}</Card.Title>
                                        <Card.Text>Source: {article.sourceName}</Card.Text>
                                        <Card.Text>Reported Country: {article.reportedCountry}</Card.Text>
                                        <Card.Text>Published: {article.publishedAt.substring(0, 10)}</Card.Text>
                                        <Card.Text>Source Country: {article.sourceCountry}</Card.Text>
                                        <Card.Text>Category: {article.category.charAt(0).toUpperCase() + article.category.slice(1)}</Card.Text>
                                        <Card.Text>Language: {article.language}</Card.Text>
                                        <Card.Text>Publication Website: <a href={`${article.websiteURL}`}>{article.websiteURL}</a></Card.Text>
                                        <a href={article.websiteURL}></a>

                                        <UrlPreview url={article.websiteURL} img={article.articleImage} />

                                        <br />
                                        <Card.Text>Description : {article.description}</Card.Text>
                                        <Card.Text>Article Author : {article.articleAuthor}</Card.Text>
                                        <Card.Text>Article Snippet : {article.articleSnippet}</Card.Text>
                                    
                                        <OverlayTrigger
                                            placement="top"
                                            delay={{ show: 250, hide: 400 }}
                                            overlay={renderCountry}
                                        >
                                            <Button href={`/countries/${article.article_country_id}`}  variant="secondary">See Related Country</Button>
                                        </OverlayTrigger> {' '}

                                        <OverlayTrigger
                                            placement="top"
                                            delay={{ show: 250, hide: 400 }}
                                            overlay={renderCharity}
                                        >
                                            <Button class="btn btn-secondary" href={`/charities/${article.article_charity_id}`} variant="secondary">See Related Charity</Button>
                                        </OverlayTrigger> {' '}
                                        
                                        {' '}
                                        <Button href={`/articles`} variant="secondary">All Articles</Button>{' '}
                                    </Card.Body>
                                </Card>
                            </Col>
                        </>

                    ): undefined
                }
            </Row>
        </Container>
    )
}

export default ArticleInstance