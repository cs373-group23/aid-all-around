import { useState, useEffect } from 'react';
import { Card, Container, Row, Col, Button, OverlayTrigger, Tooltip } from 'react-bootstrap'
import UrlPreview from '../components/UrlPreview'

const CharityInstance = (props) => {
    let id = props.match.params.id

    const [charity, setCharity] = useState([])
    const [loading, setLoading] = useState(true)
    const [countryName, setCountryName] = useState(true)
    const [articleName, setArticleName] = useState(true)

    const renderCountry = (props) => (
        <Tooltip id="button-tooltip" {...props}>
            {countryName}
        </Tooltip>
    );

    const renderArticle = (props) => (
        <Tooltip id="button-tooltip" {...props}>
            {articleName}
        </Tooltip>
    );

    useEffect(() => {
        const fetch_and_set = async () => {
            let response = await fetch(`https://www.aidallaround.me/api/charity/${id}`)
            let charity = await response.json()
            setCharity(charity)

            response = await fetch(`https://www.aidallaround.me/api/country/${charity.charity_country_id}`)
            let country = await response.json()
            setCountryName(country.name)

            response = await fetch(`https://www.aidallaround.me/api/article/${charity.charity_article_id}`)
            let article = await response.json()
            setArticleName(article.articleName)

            setLoading(false)
        }
        fetch_and_set()
    }, [])

    return (
        <Container fluid style={{
            marginTop: "20px", display: "flex", justifyContent: "center",
            alignItems: "center",
            textAlign: "center"
        }}>
            <Row noGutters>
                {!loading ?
                    (
                        <>
                            <Col>
                                <Card style={{ width: '50vw' }}>
                                    <Card.Body>
                                        <Card.Title as="h5">{charity.charityName}</Card.Title>
                                        <Card.Text>Asset Amount: {charity.assetAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Card.Text>
                                        <Card.Text>Cause Name: {charity.causeName}</Card.Text>
                                        <Card.Img src={charity.causeImage} style={{width: "100px", height: "100%"}} />
                                        <br /><br />
                                        <Card.Text>EIN: {charity.ein}</Card.Text>
                                        <Card.Text>Income Amount: {charity.incomeAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Card.Text>
                                        <Card.Text>City: {charity.mailingAddress.city}</Card.Text>
                                        <Card.Text>Mission: {charity.mission}</Card.Text>
                                        <Card.Text>Classification: {charity.nteeClassification}</Card.Text>
                                        <Card.Text>Type: {charity.nteeType}</Card.Text>
                                        <Card.Text>Rating: {charity.rating} (out of 4)</Card.Text>
                                        <Card.Img src={charity.ratingImage} style={{width: "200px", height: "100%"}} />
                                        <br /><br />
                                        <Card.Text>Tag Line: {charity.tagLine}</Card.Text>
                                        <Card.Text>Website URL: <a href={`${charity.websiteURL}`}>{charity.websiteURL}</a></Card.Text>
                                        <UrlPreview url={charity.websiteURL} img={charity.causeImage} />

                                        <br />
                                        <OverlayTrigger
                                            placement="top"
                                            delay={{ show: 250, hide: 400 }}
                                            overlay={renderCountry}
                                        >
                                            <Button href={`/countries/${charity.charity_country_id}`}variant="secondary">See Related Country</Button>
                                        </OverlayTrigger> {' '}

                                        <OverlayTrigger
                                            placement="top"
                                            delay={{ show: 250, hide: 400 }}
                                            overlay={renderArticle}
                                        >
                                            <Button class="btn btn-secondary" href={`/articles/${charity.charity_article_id}`} variant="secondary">See Related Article</Button>
                                        </OverlayTrigger> {' '}
                                        
                                        <Button href={`/charities`} variant="secondary">All Charities</Button>{' '}
                                    </Card.Body>
                                </Card>
                            </Col>
                        </>

                    ): undefined
                }
            </Row>
        </Container>
    )
}

export default CharityInstance