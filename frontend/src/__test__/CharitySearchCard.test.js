import CharitySearchCard from '../components/CharitySearchCard';
import { shallow, mount, configure } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import { render } from '@testing-library/react';

// mock data
const source = {
  charity_id: "1",
  charityName: "Safe Passage",
  nteeClassification: "International Relief",
  incomeAmount: 2360372.0,
  assetAmount: 3405983.0,
  causeName: "Development and Relief Services",
  rating: "3"
  };

describe('Test suits for CharitySearchCard', () => {
  it('should match with snapshot', () => {
    const tree = render(JSON.stringify(
      <MemoryRouter>
        <CharitySearchCard data={source} query={["Passage"]} hoverable="true" />
      </MemoryRouter>
    ))
   expect(tree).toMatchSnapshot();
  });
});