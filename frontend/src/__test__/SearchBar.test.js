import SearchBar from '../components/SearchBar';
import { shallow, mount, configure } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import { render } from '@testing-library/react';

describe('Test suits for SearchBar', () => {
	it('should match with snapshot', () => {
		const tree = render(JSON.stringify(
			<MemoryRouter>
				<SearchBar modelString="country"/>
			</MemoryRouter>
		))
		expect(tree).toMatchSnapshot();
	});
});