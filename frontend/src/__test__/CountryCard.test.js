import CountryCard from '../components/CountryCard';
import { shallow, render, mount, configure } from 'enzyme';

// mock data
const country = {
  name: "Afghanistan",
  region: "Asia",
  latlng: [33, 65],
  population: 27657145,
  incomeLevel: "Low income",
  gini: 27.8
};

describe("", () => {
  it("accepts country props", () => {
    const wrapper = mount(<CountryCard data={country} />);
    expect(wrapper.props().data).toEqual(country);
  });

  it("contains country name", () => {
    const wrapper = mount(<CountryCard data={country} />);
    expect(wrapper.text().includes('Asia')).toBe(true);
  });
});