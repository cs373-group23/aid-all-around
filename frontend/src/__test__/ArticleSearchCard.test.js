import ArticleSearchCard from '../components/ArticleSearchCard';
import { shallow, mount, configure } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import { render } from '@testing-library/react';

// mock data
const source = {
  articleName: "Grief as Guatemalan migrants killed in Mexico laid to rest",
  sourceName: "BBC News",
  reportedCountry: "Guatemala",
  publishedAt: "2021-03-15T12:58:54Z",
  sourceCountry: "gb",
  category: "general"
};

describe('Test suits for ArticleSearchCard', () => {
  it('should match with snapshot', () => {
    const tree = render(JSON.stringify(
      <MemoryRouter>
        <ArticleSearchCard data={source} query={["Guatemalan"]} hoverable="true" />
      </MemoryRouter>
    ))
   expect(tree).toMatchSnapshot();
  });
});