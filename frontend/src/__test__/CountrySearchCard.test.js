import CountrySearchCard from '../components/CountrySearchCard';
import { shallow, mount, configure } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import { render } from '@testing-library/react';

// mock data
const source = {
  name: "Afghanistan",
  region: "Asia",
  latlng: [33, 65],
  population: 27657145,
  incomeLevel: "Low income",
  gini: 27.8
};

describe('Test suits for CountrySearchCard', () => {
  it('should match with snapshot', () => {
    const tree = render(JSON.stringify(
      <MemoryRouter>
        <CountrySearchCard data={source} query={["Asia"]} hoverable="true" />
      </MemoryRouter>
    ))
   expect(tree).toMatchSnapshot();
  });
});