import { screen } from '@testing-library/react';
import App from '../App';
import { shallow, render, mount, configure } from 'enzyme';

it("renders without crashing", () => {
  shallow(<App />);
});

it("renders Account header", () => {
  const wrapper = shallow(<App />);
  const welcome = <title>Aid All Around</title>;
  expect(wrapper.contains(welcome)).toEqual(true);
});