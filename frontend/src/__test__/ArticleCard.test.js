import ArticleCard from '../components/ArticleCard';
import { shallow, render, mount, configure } from 'enzyme';

// mock data
const source = {
  articleName: "Grief as Guatemalan migrants killed in Mexico laid to rest",
  sourceName: "BBC News",
  reportedCountry: "Guatemala",
  publishedAt: "2021-03-15T12:58:54Z",
  sourceCountry: "gb",
  category: "general"
};

describe("", () => {
  it("accepts source props", () => {
    const wrapper = mount(<ArticleCard data={source} />);
    expect(wrapper.props().data).toEqual(source);
  });

  it("contains source name", () => {
    const wrapper = mount(<ArticleCard data={source} />);
    expect(wrapper.text().includes('BBC News')).toBe(true);
  });
});