import CharityComponent from '../components/CharityComponent';
import { shallow, render, mount, configure } from 'enzyme';

// mock data
const source = {
  charity_id: "1",
  charityName: "Test Charity",
  nteeClassification: "test classification",
  incomeAmount: 2000,
  assetAmount: 4000,
  causeName: "Test Cause",
  rating: "3"
  };

describe("", () => {
  it("accepts source props", () => {
    const wrapper = mount(<CharityComponent data={source} />);
    expect(wrapper.props().data).toEqual(source);
  });

  it("contains source charity_id", () => {
    const wrapper = mount(<CharityComponent data={source} />);
    expect(wrapper.text().includes('1')).toBe(true);
  });
});