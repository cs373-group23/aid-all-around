import Filter from '../components/Filter';
import { shallow, render, mount, configure } from 'enzyme';

describe("", () => {
  it("accepts filter props", () => {
    const wrapper = mount(<Filter filters={[]} setFilters={[]} name={"region"} options={["Asia", "Oceania", "Europe", "Americas", "Africa"]}/>);
    expect(wrapper.props().options).toEqual(["Asia", "Oceania", "Europe", "Americas", "Africa"]);
  });

  it("contains filter name", () => {
      const wrapper = mount(<Filter filters={[]} setFilters={[]} name={"region"} options={["Asia", "Oceania", "Europe", "Americas", "Africa"]}/>);
      expect(wrapper.text().includes('region')).toBe(true);
  });
});