import Sort from '../components/Sort'
import { shallow, render, mount, configure } from 'enzyme';

describe("", () => {
  it("accepts sort props", () => {
    const wrapper = mount(<Sort queries = {[]} setQueries = {[]} options = {["population", "latitude", "longitude", "gini"]} />);
    expect(wrapper.props().options).toEqual(["population", "latitude", "longitude", "gini"]);
  });
});