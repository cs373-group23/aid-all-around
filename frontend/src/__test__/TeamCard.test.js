import TeamCard from '../components/TeamCard';
import { shallow, render, mount, configure } from 'enzyme';

// mock data
const member = {
  src: "Test src",
  name: "Rick Ross",
  bio: "Rapper", 
  responsibilities: "backend",
  commits : 0,
  issues: 0,
};

describe("", () => {
  it("accepts country props", () => {
    const wrapper = mount(<TeamCard src={member.src} name={member.name} bio={member.bio} responsibilities={member.responsibilities} commits={member.commits} issues={member.issues}  />);
    expect(wrapper.props()).toEqual(member);
  });

  it("contains member name", () => {
    const wrapper = mount(<TeamCard src={member.src} name={member.name} bio={member.bio} responsibilities={member.responsibilities} commits={member.commits} issues={member.issues}  />);
    expect(wrapper.text().includes('backend')).toBe(true);
  });
});