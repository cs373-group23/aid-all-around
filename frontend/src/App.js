import './App.css';
import { BrowserRouter } from "react-router-dom";
import NavBar from './components/NavBar'
import Routes from './routes/routes'
import Favicon from "react-favicon";

function App() {
  return (
    <BrowserRouter>
     <title>Aid All Around</title>
     <Favicon
        url="https://png.pngtree.com/element_our/png/20181228/earth-vector-icon-png_296088.jpg"
      />
      <NavBar />
      <Routes />
    </BrowserRouter>
  );
}

export default App;
