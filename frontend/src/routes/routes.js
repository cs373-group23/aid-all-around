import Splash from '../views/splash/splash'
import Country from '../views/countries/countries'
import Charities from '../views/charities/charities'
import Articles from '../views/news/articles'
import About from '../views/about/about'
import CountryInstance from '../instances/CountryInstance'
import CharityInstance from '../instances/CharityInstance'
import ArticleInstance from '../instances/ArticleInstance'
import Search from '../views/search/search'
import Visualization from '../views/vis/visualization'
import { Route, Switch } from "react-router-dom";

const Routes = () => {
    return (
        <Switch>

            <Route path="/" exact component={Splash} />
            <Route path="/countries" exact component={Country} />
            <Route exact path='/countries/:id' component={CountryInstance} />
            <Route path="/charities" exact component={Charities} />
            <Route exact path="/charities/:id" component={CharityInstance} />
            <Route path="/articles" exact component={Articles} />
            <Route exact path="/articles/:id" component={ArticleInstance} />
            <Route path="/about" exact component={About} />
            <Route path="/search" exact component={Search} />
            <Route path="/vis" exact component={Visualization} />

        </Switch>
    )
}

export default Routes