# aid-all-around

A website to encourage people to be more informed about the state of the world today, and that provides an avenue for supporting different places and causes.


### Project Requirements
##### Member1
* Name: Ilham Thomson
* EID: iht67
* GitLab ID: ThorTheDwarf
* Estimated completion time (hours: int): 16
* Actual completion time (hours: int): 24

##### Member2
* Name: Shivang Singh
* EID:  ss82789
* GitLab ID: shivang.singh
* Estimated completion time (hours: int): 16
* Actual completion time (hours: int): 24

##### Member3
* Name: Thomas Connor Thompson
* EID: tct572
* GitLab ID: thocotho
* (Dropped Class After Phase 1)

##### Member4
* Name: Abdul Rehman Maanda
* EID: am93568
* GitLab ID: themaanda
* Estimated completion time (hours: int): 16
* Actual completion time (hours: int): 24

##### Member5
* Name: Zhiyao Bao
* EID: zb3542
* GitLab ID: zhiyao-bz
* Estimated completion time (hours: int): 16
* Actual completion time (hours: int): 24

##### Git SHA: 3bf45e97497d5dbae14c704b87722d27fca9662a

##### Project leader:
* Phase 1: Shivang Singh
* Phase 2: Ilham Thomson
* Phase 3: Abdul Rehman Maanda
* Phase 4: Zhiyao Bao

##### GitLab Pipelines: https://gitlab.com/cs373-group23/aid-all-around/-/pipelines

##### Website: https://www.aidallaround.me

##### Presentation Video: https://www.youtube.com/watch?v=4me75AiGZu8

##### Comments: 

### Guide
In the /frontend directory, you can run:
##### `npm install`
Install the related dependencies.

##### `npm run start`
Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

##### `npm test`
Runs the frontend unittests.

##### Ctrl-z to stop running.
