# run docker for frontend
docker frontend:
	docker run -it -p 3000:3000 -v $(PWD):/usr/typescript -w /usr/typescript gpdowning/typescript

docker backend:
	docker run -it -p 3000:3000 -v $(PWD):/usr/python -w /usr/python ssingh101/aidallaroundbackend
# for windows => docker run -it -p 3000:3000 -v C:/Users/Acer/aid-all-around/backend:/usr/python -w /usr/python ssingh101/aidallaroundbackend

check threads:
	ps -fA | grep python

# get git config
config:
	git config -l

# get git status
status:
	make clean
	@echo
	git branch
	git remote -v
	git status.

# download files from the Collatz code repo
pull:
	make clean
	@echo
	git pull
	git status

# upload files to the Collatz code repo
push:
	make clean
	@echo
	# git add .gitignore
	# git add .gitlab-ci.yml
	# git add makefile
	# git add README.md
	git add .
	git commit -m "another commit"
	git push
	git status

all:
# check files, check their existence with make check
CFILES :=                                 \
    .gitignore                            \
    .gitlab-ci.yml

# check the existence of check files
check: $(CFILES)

# remove temporary files
clean:
	rm -f  .coverage
	rm -f  .pylintrc
	rm -f  *.pyc
	rm -f  *.tmp
	rm -rf __pycache__
	rm -rf .mypy_cache